var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'blaze.infomailer@gmail.com',
        pass: 'login@123'
    }
}, {
    // default values for sendMail method
    from: 'blaze.infomailer@gmail.com',
    headers: {
        'My-Awesome-Header': '123'
    }
});
exports.sendMail = function(to, subject, mailBody){
    transporter.sendMail({
        to: to,
        subject: subject,
        text: mailBody
    });
};
