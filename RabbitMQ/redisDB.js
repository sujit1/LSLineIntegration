var redis = require('redis'),
    log4js = require('log4js'),
    Globals = require('./globals'),
    client;

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

exports.connect = function(host, port, cb){
    client = redis.createClient(port, host);

    client.on('connect', function(){
        cb();
    });

    client.on('error', function(err){
        logger.error('Unable to connect to Redis.' + err);
    });
};

exports.setData = function(key, obj, cb){
    client.hmset(key, obj, function(err, object){
        logger.info(object);
        cb(err, object);
    });
};

exports.getData = function(key, cb){
    client.hgetall(key, function(err, object) {
        logger.info(JSON.stringify(object));
        if(cb){ cb(err, object); }
    });
};

exports.isKeyExists = function(key, cb){
    client.exists(key, function(err, reply) {
        if (reply === 1) {
            cb(true);
            logger.info('exists');
        } else {
            logger.info('doesn\'t exist');
            cb(false);
        }
    });
};

exports.delData = function(key, cb){    
    client.del(key, function(err, reply) {
        logger.info("reply");
        cb(err, reply);
    });
};