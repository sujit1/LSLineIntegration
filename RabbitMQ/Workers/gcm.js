var gcm = require('node-gcm');
var Globals = require('../globals');
var log4js = require('log4js');

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

var GCM = function () {
    this.options = {
        priority: 'high',
        contentAvailable: true,
        delayWhileIdle: true,
        timeToLive: 3,
        dryRun: Globals.notification.gcm.sandbox
    };
    this.message = new gcm.Message(this.options);
    this.sender = new gcm.Sender(Globals.notification.gcm.apiKey); // API KEY from GCM
};



GCM.prototype.alert = function (jsonObject) {
    try {
        if (jsonObject && jsonObject.hasOwnProperty("deviceToken_ANDROID")) {
            var registrationTokens = [];
            var deviceTokens = jsonObject.deviceToken_ANDROID.split(',');
            for (var initial = 0; initial < deviceTokens.length; initial++) {
                var _device = (deviceTokens[initial]).trim();
//                logger.info("device push :" + _device+" ****** "+initial);
                if (_device.length === 152) {
//                    logger.info("device push : 152 :"+registrationTokens.length);
                    registrationTokens.push(_device);
                    logger.info("device pushed to array :" + _device);
                }
            }

            if (registrationTokens.length > 0) {
                var note = {};
                note.expiry = Math.floor(Date.now() / 1000) + 3600;
                note.title = jsonObject.message || "Blaze B One alert";
                note.message = JSON.stringify({'messageFrom': 'Blaze B One', "hub_id": (jsonObject.hub_id || ""),
                    "device_b_one_id": (jsonObject.device_b_one_id || ""), "category_type": (jsonObject.category_type || "")
                    , "category_id": (jsonObject.category_id || "")});

                this.message.addData(note);
                this.sender.sendNoRetry(this.message, {registrationTokens: registrationTokens}, function (err, response) {
                    if (err) {
                        logger.error(err);
                    }
                    else {
                        console.log(response);
                    }
                });
            }
        } else {
            logger.error("Please add device token to deviceToken key");
        }
    } catch (e) {
        logger.error("Unknown error :" + e.message);
    }

    return true;
};

// ==============================================
module.exports = new GCM();
