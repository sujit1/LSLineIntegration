var nodemailer = require('nodemailer'),
    jade = require('jade'),
    Globals = require('../globals'),
    _ = require('underscore'),
    log4js = require('log4js');

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

/*var mailOptions = {
    from: 'Fred Foo ✔ <foo@blurdybloop.com>', // sender address
    to: 'bar@blurdybloop.com, baz@blurdybloop.com', // list of receivers
    subject: 'Hello ✔', // Subject line
    text: 'Hello world ✔', // plaintext body
    html: '<b>Hello world ✔</b>' // html body
};*/

var Mailer = function(){
    _.bindAll(this, "sendMail", "genTemplate");
    if(Globals.mail.service){
            this.transporter = nodemailer.createTransport({
                service: Globals.mail.service,
                auth: {
                    user: Globals.mail.auth.user,
                    pass: Globals.mail.auth.pass
                }
            });
    }else{        
        this.transporter = nodemailer.createTransport(/*"SMTP",*/ {
            host: Globals.mail.host, // hostname
            secureConnection: Globals.mail.secureConnection, // use SSL
            port: Globals.mail.port, // port for secure SMTP
            auth: {
                    user: Globals.mail.auth.user,
                    pass: Globals.mail.auth.pass
                }, 
	    ignoreTLS: true
        });
    }
};

Mailer.prototype.sendMail = function(jsonObject){
    logger.trace("Inside sendMail");
    if(jsonObject && jsonObject.hasOwnProperty("templateName")) {
        jsonObject.html = this.genTemplate(jsonObject, Globals.template.templatePath+jsonObject.templateName);
    }
    jsonObject.from = Globals.mail.auth.user;
    this.transporter.sendMail(jsonObject, function(error, info){
        if(error) {
            logger.info("sendMail error: " + JSON.stringify(error) + " : " + error.message);
            return false;
        } else
            return true;
    });
};

Mailer.prototype.genTemplate = function( jsonObject, template ){
    logger.trace("genTemplate");
    var _template = jade.renderFile(template, jsonObject);
    if( _template )
        return _template;
    else
        return false;
};

module.exports = new Mailer();
