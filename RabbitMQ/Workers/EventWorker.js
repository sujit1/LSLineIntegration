var amqp = require('amqp'),
    Globals = require('../globals'),
    _ = require('underscore'),
    db = require('../db'),
    mySqlDB = require('../mySqlDB'),
    Config = require('../config'),
    log4js = require('log4js');

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

function EventWorker() {
    _.bindAll(this, "init", "eventWorker", "saveToDB", "getModels");
    var that = this;
    db.connect(Globals.MongoHost, Globals.MongoPort, Globals.MongoDB, function(err){
        if(err){
            logger.info("Problem in connecting MongoDB.");
        }else{
            logger.info("Connected to MongoDB.");
        }
    });
    mySqlDB.connect(Globals.MySqlHost, Globals.MySqlPort, Globals.MySqlDB, Globals.MySqlUser, Globals.MySqlPass, function (err) {
        if (err) {
            process.exit(1);
        } else {
            logger.info('connected to MySql.');
            that.connection = that.createConnection();
            that.connection.on("ready", that.init);
        }
    });
};

EventWorker.prototype.createConnection = function () {
    return amqp.createConnection({host: Globals.RabbitMQConfig.host, port: Globals.RabbitMQConfig.port, login: Globals.RabbitMQConfig.auth.user, password: Globals.RabbitMQConfig.auth.pass});
};

EventWorker.prototype.init = function () {
    var eventExchangeObj = false,
        eventQueueObj = false,
        exchangeOptions = {durable: true, autoDelete: false, type: 'fanout'},
        eventQueueOptions = {durable: true, autoDelete: false, closeChannelOnUnsubscribe: true},
        eventSubscribeOptions = {ack: true};
    eventExchangeObj = this.connection.exchange(Globals.RabbitMQConfig.EventExchangeName, exchangeOptions);
    eventQueueObj = this.connection.queue( Globals.RabbitMQConfig.EventQueueName,  eventQueueOptions);
    eventQueueObj.bind(eventExchangeObj, '#');
    eventQueueObj.subscribe(eventSubscribeOptions, this.eventWorker);
};

EventWorker.prototype.eventWorker = function (payload, headers, info, msg) {
    logger.info("Processing eventWorker payload... Inside event Queue.."); //Keep this log for debugging...
    var that = this;

    that.getModels();
    that.devices.findAll({where: {device_id: payload.device_id}})
        .then(function(device){
            device = device[0];
            if(device){
              that.deviceCategory.findAll({where: {category_id: device.category_id }})
                  .then(function(cat){
                      cat = cat[0];
                      // var keys = Config[cat.category_name.toString().replace(/ g/, "")];
                      // payload.eventObj = _.pick(payload.eventObj, keys);
                      payload.category_name = cat.category_name;
                      payload.device_radio_type = cat.device_radio_type;
                      that.saveToDB(payload, msg);
                  });
            }else{
              msg.acknowledge();
            }            
        });
 };

EventWorker.prototype.saveToDB = function (payload, msg) {
    logger.trace("In saveToDB");

    db.save('events', payload, function(err, data){
        if(err){
            logger.error(err.message);
            msg.reject(false); // reject=true, requeue=false causes dead-lettering
        }else{
            logger.info("Saved Events Data in Mongo.");
            msg.acknowledge();
        }
    });
    // mongoDB.close(function(err){
    //     if(err){
    //         logger.error(err);
    //     }else{
    //         logger.info("Closed Mongo db connection.");
    //     }
    // });
};

EventWorker.prototype.getModels = function() {
    if(this.devices){
        return ;
    }
    var Models = mySqlDB.getModels();
    this.devices = Models.Devices;
    this.deviceCategory = Models.DeviceCategory;
};

module.exports = new EventWorker();
