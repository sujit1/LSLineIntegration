/* 
 mail.js
 @Govardhan Rao Ganji - govardhanforvm@gmail.com 
 */
//Global variables
// ==============================================
var apn = require("apn");
var Globals = require('../globals');
var log4js = require('log4js');
log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');
var Notification = function () {
    this.options = {
        cert: Globals.notification.apple.cert,
        certData: null,
        key: Globals.notification.apple.key,
        keyData: null,
        ca: null,
        pfx: null,
        pfxData: null,
        passphrase: Globals.notification.apple.passphrase,
        address: Globals.notification.apple.url.notification,
        port: Globals.notification.apple.port.notification,
        rejectUnauthorized: true,
        enhanced: true,
        cacheLength: 100,
        autoAdjustCache: true,
        maxConnections: 1,
        connectionTimeout: 0,
        buffersNotifications: true,
        fastMode: false,
        legacy: false
    };
    this.apnConnection = new apn.Connection(this.options);
};
Notification.prototype.alert = function (jsonObject) {
    try {
        if (jsonObject && jsonObject.hasOwnProperty("deviceToken_IOS")) {
            var devices = [];
            var deviceTokens = jsonObject.deviceToken_IOS.split(',');
            for (var initial = 0; initial < deviceTokens.length; initial++) {
                var _device = deviceTokens[initial].trim();
                if (_device.length === 64) {
                    var device = new apn.Device(_device);
                    devices.push(device);
                    logger.info("device pushed to array :" + device);
                }
            }

            if (devices.length > 0) {
                var note = new apn.Notification();
                note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
                note.badge = 1;
                note.sound = jsonObject.sound || "ping.aiff";
                note.alert = jsonObject.message || "Blaze B One alert";
                note.payload = {'messageFrom': 'Blaze B One', "hub_id": (jsonObject.hub_id || ""),
                    "device_b_one_id": (jsonObject.device_b_one_id || ""), "category_type": (jsonObject.category_type || "")
                    , "category_id": (jsonObject.category_id || "")};
                this.apnConnection.pushNotification(note, devices);
                logger.info("Notifications pushed to server.");
            }
        } else {
            logger.error("Please add device token to deviceToken key");
        }
    } catch (e) {
        logger.error("Invalid IOS device token: " + e.message);
    }

    return true;
};
Notification.prototype.feedback = function () {
    var options = this.options;
    options.address = Globals.notification.apple.url.feedback;
    options.port = Globals.notification.apple.port.feedback;
    options.batchFeedback = true;
    options.interval = 1000;
    options.production = false;
    options.feedback = true;
    var feedback = new apn.Feedback(options);
    feedback.on("feedback", function (devices) {
        devices.forEach(function (item) {
            var device = new apn.Device(item.device.token);
            // Do something with item.device and item.time;
            logger.info(item.time + " >>  >> " + device);
        });
    });
    return true;
};
// ==============================================
module.exports = new Notification();

