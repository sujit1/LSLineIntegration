var Config = function () {};

Config.prototype.DropCam = [
    "Device Unique ID by B.One",
    "Software_Version",
    "Structure_id",
    "Where_id",
    "name",
    "name_Long",
    "is_online",
    "is_streaming",
    "is_audio_input",
    "last_is_online",
    "is_video_history_enabled",
    "web_url",
    "app_url",
    "last_event",
    "has_sound",
    "has_motion",
    "start_time",
    "end_time",
    "web_url",
    "app_url",
    "image_url",
    "animated_image",
];


module.exports = new Config();