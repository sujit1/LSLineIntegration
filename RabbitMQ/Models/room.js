var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    room_id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    room_name: { type: Sequelize.STRING },
    room_description: { type: Sequelize.STRING },
    create_date: { type: Sequelize.DATE, defaultValue: Sequelize.NOW }
  };
};

exports.getTableName = function(){
  return "b1_room";
};