var Path = require('path');

function Globals() {
};

Globals.prototype.MailerAuthenticationSubject = "Thanks for Registring to B.One App";
Globals.prototype.MailerAuthenticationBody = "Hi! your registered Email: mailId, authentication: code";
Globals.prototype.MailerInviteGuestSubject = "You got B.One App invitation from sender";
Globals.prototype.MailerInviteBody = "Hi! your registered Email: mailId, Password: code";
Globals.prototype.MailerSendPasswordSubject = "Forgot Password Request";
Globals.prototype.MailerSendPasswordBody = "Hi! your password has been reset. Please use the following code for login is code";

Globals.prototype.appRoot = Path.resolve(__dirname);

//RabbitMQ configuration
Globals.prototype.RabbitMQConfig = {
    host: 'localhost',
    port: 5672,
    auth: {
//       Current production server
      user: 'guest',
    pass: 'guest'
//        Old production server
        //  user: 'admin',
        //  pass: 'bz@123'
    },
    ExchangeName: "TestMessageExchange",
    QueueName: "TestMessageQueue",
    PushNotificationExchangeName: "PushNotificationTestMessageExchange",
    PushNotificationQueueName: "PushNotificationQueue",
    EventExchangeName: "TestEventMessageExchange",
    EventQueueName: "TestEventQueue"
};

// Email configuration
// ==============================================
Globals.prototype.mail = {
    //service: 'smtp.gmail.com',
    host: 'smtpout.secureserver.net',
    port: 80,
    secureConnection: false,
    auth: {
        pass: 'LiveSmart1219',
        user: 'Support@livesmart.jp'
    }
};

// Template configuration
// ==============================================
Globals.prototype.template = {
    templatePath: Globals.prototype.appRoot + '/Workers/Templates/'
};

// Push notification configuration
// ==============================================
Globals.prototype.notification = {
    apple: {
//		cert: 'RabbitMQ/cert/PushNotifsCert.pem',
        // cert: Globals.prototype.appRoot+ '/cert/dev_cert.pem',
        cert: Globals.prototype.appRoot + '/cert/Cer_Push24April.pem',
//		key: 'RabbitMQ/cert/PushNotifsKey.pem',
        // key: Globals.prototype.appRoot + '/cert/dev_key.pem',
        key: Globals.prototype.appRoot + '/cert/Key_Push24April.pem',
//		passphrase: 'PurpleCUBE@2015',
        // passphrase: 'Blaze@auto2016',
        passphrase: 'Admin1234#',
        // pfx: Globals.prototype.appRoot + '/cert/Blaze-APNS-Dev.p12',
        pfx: Globals.prototype.appRoot + '/cert/Push24April.p12',
        url: {
            notification: 'gateway.push.apple.com', // for prod use, gateway.push.apple.com, for developement use, gateway.sandbox.push.apple.com
            feedback: 'feedback.push.apple.com' //for prod use, feedback.push.apple.com, for developement use, feedback.sandbox.push.apple.com
        },
        port: {
            notification: 2195,
            feedback: 2196
        }
    },
    gcm: {
        //apiKey: "AIzaSyBUaJB66tyBctPeT9vj3Ahuv0KB2WGW3tk",
        //apiKey: "AIzaSyAMul2xcfiZUxqg6kBdFC7rsM6pqPcSDFc",
        apiKey: "AIzaSyD7_4A_RX_fJGdPt4adn-JZGlKQmyy7c64",
        sandbox: false
    }
};

//Mysql DB configurations
Globals.prototype.MySqlPort = 3306;
Globals.prototype.MySqlDB = "BlazeDB";
Globals.prototype.MySqlUser = "root";
//Current production server
Globals.prototype.MySqlHost = '172.31.33.246';//'localhost';
Globals.prototype.MySqlPass = "blaze";
//        Old production server
//  Globals.prototype.MySqlHost = '172.31.12.148';
//  Globals.prototype.MySqlPass = "bl@z3";

//Mongo DB configurations
Globals.prototype.MongoPort = 27017;
//Current production server
Globals.prototype.MongoHost = '172.31.33.246';//'localhost';
Globals.prototype.MongoDB = 'blazedb';
//        Old production server
//  Globals.prototype.MongoHost = '172.31.12.148';
// Globals.prototype.MongoDB = 'test1';

//Redis DB configurations
Globals.prototype.RedisPort = 6379;
//Current production server
Globals.prototype.RedisHost = '172.31.33.246';//'localhost';
//        Old production server
//  Globals.prototype.RedisHost = '172.31.12.148';

Globals.prototype.LoggerConfig = {
    appenders: [
        {type: 'console'},
        {
            "type": "file",
            "filename": Globals.prototype.appRoot + "/logs/log_file.log",
            "maxLogSize": 10485760,
            "backups": 10,
            "category": "relative-logger"
        }
    ]
};
module.exports = new Globals();
