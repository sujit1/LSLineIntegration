var Path = require('path');

function Globals() {
};
Globals.prototype.MailerAuthenticationSubject = "Live Smart - Registration Successful";
Globals.prototype.MailerInviteGuestSubject = "Live Smart- Invitation to collaborate";
Globals.prototype.MailerForgotPasswordSubject = "Live Smart - Forgot password reset";
Globals.prototype.MailerReportSubject = "Live Smart - Registered Report";
//DB configurations
Globals.prototype.MongoPort = 27017;
//Current production server
Globals.prototype.MongoHost = 'localhost';//'localhost'; // '54.86.166.212';
Globals.prototype.MongoDB = 'blazedb';
//        Old production server
//  Globals.prototype.MongoHost = '172.31.12.148'; //' 52.90.18.230';
// Globals.prototype.MongoDB = 'test1';

//RabbitMQ configuration
Globals.prototype.RabbitMQConfig = {
    host: '127.0.0.1',
    port: 5672,
    auth: {
//        Current production server
     user: 'guest',
      pass: 'guest'
//        Old production server
       //  user: 'admin',
        // pass: 'bz@123'
    },
    ExchangeName: "TestMessageExchange",
    QueueName: "TestMessageQueue",
    PushNotificationExchangeName: "PushNotificationTestMessageExchange",
    PushNotificationQueueName: "PushNotificationQueue",
    EventExchangeName: "TestEventMessageExchange",
    EventQueueName: "TestEventQueue"
};

// Template configuration
// ==============================================
Globals.prototype.template = {
    forgotPassword: 'forgotPassword.jade',
    registration: 'registration.jade',
    invitation: 'invitation.jade',
    resolved: 'resolved.jade',
    created: 'created.jade'
};

//Mysql DB configurations
Globals.prototype.MySqlPort = 3306;
Globals.prototype.MySqlDB = "BlazeDB";
Globals.prototype.MySqlUser = "root";
//Current production server
Globals.prototype.MySqlHost = 'localhost';//'localhost'; // '54.86.166.212';
Globals.prototype.MySqlPass = "blaze";
//        Old production server
//  Globals.prototype.MySqlHost = '172.31.12.148'; //' 52.90.18.230';
 //Globals.prototype.MySqlPass = "bl@z3";

//Redis DB configurations
Globals.prototype.RedisPort = 6379;
//Current production server
Globals.prototype.RedisHost = 'localhost';//'localhost'; // '54.86.166.212';
//        Old production server
//  Globals.prototype.RedisHost = '172.31.12.148'; //' 52.90.18.230';

// Mail images
Globals.prototype.MailLogo = "/images/mail_japan.jpg";
Globals.prototype.appRoot = Path.resolve(__dirname);
Globals.prototype.maxContacts = 8;
Globals.prototype.maxBOneIdChars = 4;

Globals.prototype.LoggerConfig = {
    appenders: [
        {type: 'console'},
        {
            "type": "file",
            "filename": Globals.prototype.appRoot + "/logs/log_file.log",
            "maxLogSize": 10485760,
            "backups": 10,
            "category": "relative-logger"
        }
    ]
};

Globals.prototype.adminMail = "vivek@blazeautomation.com";
//Globals.prototype.adminMail = "sujit@blazeautomation.com";


Globals.prototype.maxMongoRecordCount = 30000;
Globals.prototype.resetMongoRecordCount = 10000;

Globals.prototype.maxDummyHubsDigitLength = 10;

Globals.prototype.graceTime = 120000; // in milliseconds used for hub on off status schedule time
Globals.prototype.beseyeDevClientId="5cbfa2ea68a7b70044bd07431a830149e58d3397e748ae2dc86ca51765b4a30d";
Globals.prototype.beseyeDevClientSecret="7e26c2285616481d56dc708ea3da08938305a59ddd981ba39b95cea8d7dc8c8d";

module.exports = new Globals();
