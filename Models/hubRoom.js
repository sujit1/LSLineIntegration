var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    hub_room_id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    room_id: { type: Sequelize.CHAR(36) },
    hub_id: { type: Sequelize.STRING },
    room_enabled: { type: Sequelize.BOOLEAN, defaultValue: true }
  };
};

exports.getTableName = function(){
  return "b1_hub_room";
};
