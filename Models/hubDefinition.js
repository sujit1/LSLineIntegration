var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    hub_id: { type: Sequelize.STRING },
    device_name: { type: Sequelize.STRING },
    device_desc: { type: Sequelize.STRING },
    hub_longitude: { type: Sequelize.FLOAT },
    hub_latitude: { type: Sequelize.FLOAT },
    // no_of_rooms: { type: Sequelize.INTEGER, defaultValue: 0 }
  };
};

exports.getTableName = function(){
  return "b1_hub_definition";
};