var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    alexa_id: { type: Sequelize.CHAR(36), primaryKey: true, defaultValue: Sequelize.UUIDV4 },
    user_id: { type: Sequelize.STRING, defaultValue: null },
    email_id: { type: Sequelize.STRING, defaultValue: null },
    state: { type: Sequelize.STRING, defaultValue: null },
    client_id: { type: Sequelize.STRING, defaultValue: null},
    scope: { type: Sequelize.STRING, defaultValue: null },
    response_type: { type: Sequelize.STRING, defaultValue: null },
    session_id: { type: Sequelize.STRING, defaultValue: null},
    access_token: { type: Sequelize.STRING, defaultValue: null },
    refresh_token: { type: Sequelize.STRING, defaultValue: null },
    count: { type: Sequelize.INTEGER, defaultValue: 1 }
  };
};

exports.getTableName = function(){
  return "b1_alexa_user";
};
