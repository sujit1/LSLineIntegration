var Sequelize = require('sequelize');

exports.getSchema = function() {
  return {
    line_id: { type: Sequelize.STRING, primaryKey: true },
    user_id: { type: Sequelize.STRING, defaultValue: null },
    email_id: { type: Sequelize.STRING, defaultValue: null },
    hub_id: { type: Sequelize.STRING, defaultValue: null },
    state: { type: Sequelize.STRING, defaultValue: null },
    count: { type: Sequelize.INTEGER, defaultValue: 1 }
  };
};

exports.getTableName = function(){
  return "b1_line_user";
};
