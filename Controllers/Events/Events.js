var express = require('express')
, router = express.Router()
, _ = require('underscore')
, Globals = require('../../globals')
, QueueHandler = require('../Commons/QueueHandler')
, db = require('../../db')
, mysqlDb = require('../../mySqlDB')
, redisDb = require('../../redisDB')
, User = require('../User/Users')
, Utils = require('../Commons/Utils')
, request = require("request")
, log4js = require('log4js');

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

function Events() {
  _.bindAll(this, "sendEvent", "getModels", "getEvent", "getAllEvents", "sendNotification", "validateHubDevice", "setState", "getLatestState"
  , "getOldStates", "sendGetEventRes", "saveToHistory", "resetHubQueue", "setHubState", "getLastStatesOnHub", "setSmsStatus", "updateSmsStatus"
  , "getSmsStatus", "getHubStatus", "getEventsCount", "getStatus","beseye","getEnergyValues", "fetchHubOnOff", "insertStatus","ninjaLock"
  , "beseyeTriggerApi","callTrigger" ,"besSendEvent","besSetState","besInsertState","bessaveToHistory","ninjaDoLock","ninjaDoUnlock","ninjaStatus"
  , "ninjaResult", "besStatus","callbeseyeStatus","sendbesNotification","besInsertStateone","besSetStateone","bessaveToHistoryone"
  ,"besInsertStatetwo","besSetStatetwo","bessaveToHistorytwo");

  router.post('/sendEvent',User.validateSession,this.sendEvent);
  router.post('/getEvent', User.validateSession, this.getEvent);
  router.post('/getAllEvents', User.validateSession, this.getAllEvents);
  router.post('/SetStatus', User.validateSession, this.setState);
  router.post('/getLatestStatus', User.validateSession, this.getLatestState);
  router.post('/getOldStatus', User.validateSession, this.getOldStates);
  router.post('/resetHubQueue', User.validateSession, this.resetHubQueue);
  router.post('/sendNotification', User.validateSession, this.sendNotification);
  router.post('/SetHubStatus', User.validateSession, this.setHubState);
  router.post('/setSmsStatus', User.validateSession, this.setSmsStatus);
  router.post('/updateSmsStatus', User.validateSession, this.updateSmsStatus);
  router.post('/getSmsStatus', User.validateSession, this.getSmsStatus);
  router.post('/getHubStatus', User.validateSession, this.getHubStatus);
  router.post('/getLastNStates', User.validateSession, this.getLastStatesOnHub);
  router.post('/getEventsCount', User.validateSession, this.getEventsCount);
  router.post('/beseyeCallback',this.beseyeProd);
  router.post('/beseyeProdCallback',this.beseye);
  router.post('/getEnergyValues', User.validateSession, this.getEnergyValues);
  router.post('/fetchHubOnOff', this.fetchHubOnOff);
  router.post('/ninjaLock',this.ninjaLock);
  router.post('/ninjaDoLock',this.ninjaDoLock);
  router.post('/ninjaDoUnlock',this.ninjaDoUnlock);
  router.post('/ninjaStatus',this.ninjaStatus);
  router.post('/ninjaResult',this.ninjaResult);
  router.post('/besStatus',this.besStatus);
  return router;
};

Events.prototype.beseyeProd = function (req, res) {

  res.send({status: 0, message: "Data received sucessfully"});
};

Events.prototype.getAllEvents = function (req, res) {
  this.sendGetEventRes(res, req, null);
};

Events.prototype.getEvent = function (req, res) {
  //    this.sendGetEventRes(res, req, 1);

  //Doc :  updating the hubs status in the b1_hubs table
  var that = this;
  that.getModels();
  //  logger.error("hub _id"+ req.body.hub_id);
  that.hubModel.findAll({where: {hub_id: req.body.hub_id }})
  .then(function(user){
    if(user.length == 0){
      res.send({status: 0, message: "hub Does not exist."});
    }else {
      //      logger.info("my hub data"+ JSON.stringify(user[0]));
      req.body.gmt = req.body.gmt || "+5:30";
      // var gmt = Utils.getTimeStamp(new Date());
      //      logger.info("time is :::"+gmt);
      user[0].update({hub_status: "1","hub_status_date" : ""+(new Date())});
      //res.send({status: 1, message: "User address updated successfully."});
      that.sendGetEventRes(res, req, 1);
    }
  })
  .catch(function(err){
    //  res.send({status: 0, message: err.message });
    logger.error(""+err.message)
  });
};

Events.prototype.sendGetEventRes = function (res, req, idx) {
  var that = this;
  var hub_id = req.body.hub_id, device_b_one_id = req.body.device_b_one_id, count = req.body.count;
  req.body.gmt = req.body.gmt || "+5:30";
  var gmt = this.calcTime(req.body.gmt);

  var query = { hub_id: hub_id };
  if(!count){
    query.event_status = "Created";
  }
  if(device_b_one_id){
    query.device_b_one_id = device_b_one_id;
  }
  db.findSortedObjects('events', query, {timestamp: 1}, function(err, data){
    if(err){
      res.send({status: 0, message: "Something went wrong. Please contact admin.", gmt: gmt});
    }else{
      if(idx){
        data = data[0];
        // data.timestamp = Utils.getTimeStamp(data.timestamp);
      }else if(count){
        data = data.reverse().splice(0, count);
        // for(var i = 0; i < data.length; i++){
        //     data[i].timestamp = Utils.getTimeStamp(data[i].timestamp);
        // }
      }
      res.send({status: 1, message: "Data retrieved successfully.", data: data, gmt: gmt});
    }
  });
};

Events.prototype.calcTime = function(offset) {
  if(offset.indexOf(":") != -1){
    var sign = offset[0];
    offset = offset.replace("+", "").replace("-", "").split(":");
    offset[1] = offset[1] || 0;
    offset = sign + ((parseInt(offset[0])*60 + parseInt(offset[1])) / 60.0);
  }

  // create Date object for current location
  var date = new Date();

  // convert to msec
  // add local time zone offset
  // get UTC time in msec
  var utc = date.getTime() + (date.getTimezoneOffset() * 60000);

  // create new Date object for different city
  // using supplied offset
  var newDate = (new Date(utc + (3600000 * offset))).toString();

  // converting the date format to "Sat Jul 23 12:11:05 2016"
  newDate = newDate.split(" ").splice(0, 5);

  var temp = newDate[4];
  newDate[4] = newDate[3];
  newDate[3] = temp;

  return newDate.join(" ");
  // return time as a string
  // return newDate.toLocaleString();
};

Events.prototype.sendEvent = function (req, res) {
  var that = this
  , hub_id = req.body.hub_id
  , device_b_one_id = req.body.device_b_one_id;

  if(req.body.reqObject && typeof(req.body.reqObject) == "string"){
    req.body.reqObject = JSON.parse(req.body.reqObject);
  }
  //res.send({status: 0, message: "No such B.One device connected to hub."});
  that.getModels();
  that.validateHubDevice(hub_id, device_b_one_id, res, function(device_id){
    QueueHandler.eventQueue({
      hub_id: hub_id, // id of the hub
      device_b_one_id: device_b_one_id, // unique B.One device id
      origin_id: req.body.origin_id,
      device_id: device_id,
      eventObj: req.body.reqObject || {},
      timestamp: new Date(),
      event_status: "Created"
    });
    res.send({status: 1, message: "Message is pushig to queue."});
  });
};

Events.prototype.sendNotification = function(req, res){
  var that = this
  , hub_id = req.body.hub_id
  , device_b_one_id = req.body.device_b_one_id
  , message = req.body.reqObject.message;

  that.getModels();
  that.validateHubDevice(hub_id, device_b_one_id, res, function(device_id){
    that.deviceModel.findAll({where: {device_id: device_id}})
    .then(function(data){
      that.deviceCategoryModel.findAll({where: {category_id: data[0].category_id}})
      .then(function(cat){
        cat = cat[0];
        var query = {hub_id: hub_id};
        query[cat.category_type == "Devices" ? "device": cat.category_type] = "1";
        that.userHubModel.findAll({where: query})
        .then(function(usrData){
          var userIds = _.pluck(usrData, "user_id");
          that.userDevModel.findAll({where: {user_id: {$in: userIds}, act_status: true}})
          .then(function(rec){
            if(rec.length === 0){
              res.send({status: 0, message: "App is not installed in any Mobile." });
              return ;
            }
            that.hubDefModel.findAll({where: {hub_id: hub_id}})
            .then(function(hub){
              that.roomModel.findAll({where: {room_id: data[0].room_id}})
              .then(function(room){
                //   message = hub[0].device_name + ":" + room[0].room_name + ":" + data[0].device_name + " :" + message;


                switch(device_b_one_id)
                {
                  case "0000":
                   // message =  "Security set to " + "\"" + message + " for " + hub[0].device_name ;
                  message = hub[0].device_name + ":" + room[0].room_name + ":" + data[0].device_name + ":" + message;  //for japan
                    break;
                  case "NOTI":
                    message =  message;
                    break;
                  default:
                   // message = data[0].device_name + " " + "\"" + message +  "\"" + " in " + room[0].room_name + " of " + hub[0].device_name ;
                   message = hub[0].device_name + ":" + room[0].room_name + ":" + data[0].device_name + ":" + message; // for japan
                }

                var android = _.chain(rec)
                .reject(function(i){return i.device_type == "IOS"})
                .pluck("app_device_token_id")
                .value();
                var ios = _.chain(rec)
                .reject(function(i){return i.device_type == "ANDROID"})
                .pluck("app_device_token_id")
                .value();
                QueueHandler.pushNotification({
                  message: message,
                  hub_id: hub_id,
                  device_b_one_id: device_b_one_id,
                  deviceToken_IOS: ios.join(","),
                  deviceToken_ANDROID: android.join(","),
                  category_type: cat.category_type,
                  category_id: cat.category_id
                });
                res.send({status: 1, message: "PushNotification is updated to server." });

                that.lineModel.findAll({where: {hub_id: hub_id}})
               .then(function(linedata){

                    if(linedata.length>0)
                    {
                         var lineuserid=linedata[0].line_id;
                         var jsondata={

                                        "hub_id":hub_id,
                                        "lineId":lineuserid,
                                        "message":message,

                                       }
                              logger.info("json data is"+JSON.stringify(jsondata));

                                var options = {
                                                      url: "https://line.b1automation.com/webhook/lineNotify",
                                                      method: 'POST',
                                                      port:443,
                                                      headers: {
                                                        'Content-Type': 'application/json',
                                                        'Accept':'application/json',

                                                      },
                                                      json:jsondata
                                               };

                       request(options, function(err, res, resbody){

                                if(!err)
                                {

                               logger.info("line notification sent sucessfully"+JSON.stringify(resbody));

                                }

                        });
                  }

               });






              });
            });

          })
          .catch(function(err){
            res.send({status: 0, message: err.message });
          });
        });
      });
    })
    .catch(function(err){
      res.send({status: 0, message: err.message });
    });
  });
};

Events.prototype.sendbesNotification = function(req){
  var that = this
  , hub_id = req.hub_id
  , device_b_one_id = req.device_b_one_id
  , message = req.message
   , device_id=req.device_id;

  that.getModels();
  that.deviceModel.findAll({where: {device_id: device_id}})
  .then(function(data){
    that.deviceCategoryModel.findAll({where: {category_id: data[0].category_id}})
    .then(function(cat){
      cat = cat[0];
      var query = {hub_id: hub_id};
      query[cat.category_type == "Devices" ? "device": cat.category_type] = "1";
      that.userHubModel.findAll({where: query})
      .then(function(usrData){
        var userIds = _.pluck(usrData, "user_id");
        that.userDevModel.findAll({where: {user_id: {$in: userIds}, act_status: true}})
        .then(function(rec){
          if(rec.length === 0){
            logger.info ("App is not installed in any Mobile." );
            return ;
          }
          that.hubDefModel.findAll({where: {hub_id: hub_id}})
          .then(function(hub){
            that.roomModel.findAll({where: {room_id: data[0].room_id}})
            .then(function(room){
               message = hub[0].device_name + ":" + room[0].room_name + ":" + data[0].device_name + ":" + message + " detected";
               var android = _.chain(rec)
               .reject(function(i){return i.device_type == "IOS"})
               .pluck("app_device_token_id")
               .value();
               var ios = _.chain(rec)
               .reject(function(i){return i.device_type == "ANDROID"})
               .pluck("app_device_token_id")
               .value();
               QueueHandler.pushNotification({
                 message: message,
                 hub_id: hub_id,
                 device_b_one_id: device_b_one_id,
                 deviceToken_IOS: ios.join(","),
                 deviceToken_ANDROID: android.join(","),
                 category_type: cat.category_type,
                 category_id: cat.category_id
               });
               logger.info( "PushNotification is updated to server." );
                //that.besSetState(req);
                that.lineModel.findAll({where: {hub_id: hub_id}})
     .then(function(linedata){
       if(linedata.length>0)
       {
         var lineuserid=linedata[0].line_id;
         var jsondata={

                        "hub_id":hub_id,
                        "lineId":lineuserid,
                        "message":message,

                      }
              logger.info("json data is"+JSON.stringify(jsondata));
              var options = {
                                    url: "https://line.b1automation.com/webhook/lineNotify",
                                    method: 'POST',
                                    port:443,
                                    headers: {
                                      'Content-Type': 'application/json',
                                      'Accept':'application/json',

                                    },
                                    json:jsondata
                             };
                             request(options, function(err, res, resbody){

                                      if(!err)
                                      {

                                     logger.info("line notification bes sent sucessfully"+JSON.stringify(resbody));
                                      that.besSetState(req);
                                      }
                                      else {
                                        that.besSetState(req);
                                      }

                              });

       }
       else {
          that.besSetState(req);
       }



     })
     .catch(function(err){
         that.besSetState(req);
         logger.info( "error occured in PushNotification " );
     });




            });
          });
        })
        .catch(function(err){
            logger.info( "error ouured in PushNotification " );
        });

      });

    });

  });
};




Events.prototype.validateHubDevice = function(hub_id, device_b_one_id, res, cb) {
  var that = this;

  that.getModels();
  that.hubRoomModel.belongsTo(that.deviceModel, {foreignKey: 'room_id', targetKey: 'room_id'});
     that.hubRoomModel.findAll({
     where: {hub_id: hub_id},
     include: [{
       model: that.deviceModel,
       attributes: ['device_id', 'device_b_one_id']
     }]
   })
   .then(function(arr){
     if(arr.length){
       var dev = _.chain(arr)
                  .pluck('Device')
                  .flatten()
                  .pluck("device_b_one_id")
                  .values();
       var idx = dev.indexOf(device_b_one_id)
       if(idx === -1){
         res.send({status: 0, message: "No such B.One device is connected to hub."});
       }else{
         cb(arr[idx].dataValues.Device.dataValues.device_id);
       }
     }else{
       res.send({status: 0, message: "Hub has no devices connected. Please make sure hub is added and devices are added to hub."});
     }
   })
   .catch(function(err){
     res.send({status: 0, message: err.message });
   });
  // that.hubRoomModel.findAll({where: {hub_id: hub_id}})
  // .then(function(hubRooms){
  //   if(hubRooms.length === 0){
  //     res.send({status: 0, message: "Hub has no devices connected. Please make sure hub is added and devices are added to hub."});
  //   }else{
  //     var rooms = _.pluck(hubRooms, "room_id");
  //     that.deviceModel.findAll({where: {room_id: {$in: rooms}}})
  //     .then(function(devices){
  //       if(devices.length === 0){
  //         res.send({status: 0, message: "Hub has no devices connected."});
  //       }else{
  //         var dev = _.pluck(devices, "device_b_one_id");
  //         var idx = dev.indexOf(device_b_one_id)
  //         if(idx === -1){
  //           res.send({status: 0, message: "No such B.One device is connected to hub."});
  //         }else{
  //           cb(devices[idx].dataValues.device_id);
  //         }
  //       }
  //     });
  //   }
  // })
  // .catch(function(err){
  //   res.send({status: 0, message: err.message });
  // });
};

Events.prototype.setState = function(req, res) {

//  logger.trace("setState start: " + JSON.stringify(req.body));
  var that = this
  , obj = req.body.reqObject || {}
  , event_id = req.body._id;

  var data_delete_id;// variable for storing the  ids  to be removed
  var delete_ids=[];// array declaration
  var modified = Object.keys(obj);
  obj.hub_id = req.body.hub_id;
  obj.device_b_one_id = req.body.device_b_one_id;
  obj.user_id = req.body.user_id;
  obj.timestamp = new Date();
  obj.modified_by = req.body.origin_id; // origin_id = 0 means hub else app
  modified.push("timestamp");

  that.validateHubDevice(obj.hub_id, obj.device_b_one_id, res, function(device_id){
    obj.device_id = device_id;
    db.findSortedObjects('states', {device_id: obj.device_id}, {timestamp: -1}, function(err, data){
      if(err){
        res.send({status: 0, message: "Something went wrong. State is not updated. Please contact admin." });
      }else if(!data.length){
        obj.modified = [];
        obj.device_record_count = 1;
        that.saveToHistory(res, null, obj, event_id);
      }else{
        data = data[0];
        delete data._id;
        delete obj._id;
        data.modified = modified;
        _.each(obj, function(v, k) { data[k] = v; });
        if (data.device_record_count) {
          that.insertStatus(data, event_id, res);
        }else{
          db.findCountByQuery('states', {device_id: device_id}, function(err, count){
            if(err){
              res.send({status: 0, message: "Something went wrong. State is not updated. Please contact admin." });
              return false;
            }
            data.device_record_count = count;
            that.insertStatus(data, event_id, res);
          });
        }
      }
    }, 1);
  });
};

Events.prototype.insertStatus = function(obj, event_id, res){
  var that = this;
  if(obj.device_record_count >= Globals.maxMongoRecordCount){
    db.findFieldsByObjects('states', {device_id: obj.device_id}, { _id: 1 }, function(err, data){
      if(err){
        res.send({status: 0, message: "Something went wrong. State is not updated. Please contact admin." });
        return false;
      }
      data = _.map(function(doc) { return doc._id; });
      db.removeManyByObject("states", data, function(err, resData){
        if(err){
          res.send({status: 0, message: "Something went wrong. records are not deleted. Please contact admin." });
        }else{
          obj.device_record_count = (Globals.resetMongoRecordCount + 1);
          that.saveToHistory(res, null, obj, event_id);
        }
      });
    }, (Globals.maxMongoRecordCount - Globals.resetMongoRecordCount));
  }else{
    obj.device_record_count = (obj.device_record_count + 1);
    that.saveToHistory(res, null, obj, event_id);
  }
}

Events.prototype.setHubState = function(req, res) {
   // logger.trace("setHubState start: " + JSON.stringify(req.body));
    var that = this
        , modified = Object.keys(req.body.reqObject)
        , obj = req.body.reqObject;
        obj.hub_id = req.body.hub_id;
        obj.user_id = req.body.user_id;
        obj.timestamp = new Date();

    modified.push("timestamp");
    db.findSortedObjects('hubStates', {"hub_id": obj.hub_id}, {timestamp: -1}, function(err, data){
        if(err){
            res.send({status: 0, message: "Something went wrong. State is not updated. Please contact admin." });
        }else{
            if(data.length === 0){
                obj.modified = [];
                that.saveToHistory(res, err, obj, null, "hubStates");
            }else{
                data = data[0];
                delete data._id;
                delete obj._id;
                data.modified = modified;
                var keys = Object.keys(obj);
                for(var i in keys){
                    data[keys[i]] = obj[keys[i]];
                }
                that.saveToHistory(res, err, data, null, "hubStates");



      }
    }
  });
};

Events.prototype.saveToHistory = function(res, err, data, event_id, _collectionName) {
  _collectionName = _collectionName || "states";
  if(err){
    res.send({status: 0, message: "Something went wrong. State is not updated. Please contact admin." });
  }else{
    db.save(_collectionName, data, function(err, hisData){
      if(err){
        res.send({status: 0, message: "Something went wrong. State is not updated. Please contact admin." });
      }else{
        res.send({status: 1, message: "State is updated successfully." });
        if(event_id){
          db.update('events', event_id, { "event_status": "Processed" }, function(err){
            //  logger.info("Deleted record with id: " + event_id);
          });
        }
      }
    });
  }
};

Events.prototype.resetHubQueue = function(req, res) {
  var that = this
  , hub_id = req.body.hub_id;

  db.updateByQuery('events', {hub_id: hub_id}, { "event_status": "Deleted" }, function(err){
    if(err){
      res.send({status: 0, message: "Something went wrong. Queue is not reset. Please contact admin."});
    }else{
      res.send({status: 1, message: "Resetting queue done successfully." });
    }
  });
};

Events.prototype.getLatestState = function(req, res) {
  var that = this
  , hub_id = req.body.hub_id
  , device_b_one_id = req.body.device_b_one_id;

  that.validateHubDevice(hub_id, device_b_one_id, res, function(device_id){
    that.getStatus('states', res, {"device_id": device_id.toString()}, 1);
  });
};

Events.prototype.getOldStates = function(req, res) {
  var that = this
  , hub_id = req.body.hub_id
  , device_b_one_id = req.body.device_b_one_id
  , count = req.body.count;

  that.validateHubDevice(hub_id, device_b_one_id, res, function(device_id){
    that.getStatus('states', res, {"device_id": device_id.toString()}, count);
  });
};

Events.prototype.getHubStatus = function(req, res) {
  var that = this
  , hub_id = req.body.hub_id
  , count = req.body.count;

  that.getStatus('hubStates', res, {"hub_id": hub_id.toString()}, count, req.body.onlyModified);
};

Events.prototype.getStatus = function(_collectionName, res, query, count, onlyModified) {
  var that = this;
  db.findSortedObjects(_collectionName, query, {timestamp: -1}, function(err, data){
    if(err){
      res.send({status: 0, message: "Something went wrong. Unable to retrive state. Please contact admin." });
    }else{
      if(data.length === 0){
        res.send({status: 0, message: "No status record available yet." });
      }else{
        if(count){
          if(count === 1){
            data[0].timestamp = Utils.getTimeStamp(data[0].timestamp);
            data = data[0];
          }else{
            data = data.splice(0, count);
            for(var i = 0; i < data.length; i++){
              data[i].timestamp = Utils.getTimeStamp(data[i].timestamp);
            }
          }
        }
        if(onlyModified == true){
          b = [];
          for(var i in data){
            if(data[i].modified.length === 0){
              delete data[i].modified;
              b.push(data[i]);
            }else{
              var c = {};
              for(var j in data[i].modified){
                c[data[i].modified[j]] = data[i][data[i].modified[j]];
              }
              b.push(c);
            };
          }
          data = b;
        }

        res.send({status: 1, message: "State is retrieved successfully.", data: data });
      }
    }
  });
};

Events.prototype.getLastStatesOnHub = function(req, res) {
  var that = this
  , count = req.body.count
  , query = {}
  , key_to_fetch = req.body.key_to_fetch;

  query.hub_id = req.body.hub_id;
  if(req.body.device_b_one_id){
    query.device_b_one_id = req.body.device_b_one_id;
  }

  db.findSortedObjects('states', query, {timestamp: -1}, function(err, _data){
    if(err){
      res.send({status: 0, message: "Something went wrong. Unable to retrive state. Please contact admin." });
    }else{
      if(_data.length === 0){
        res.send({status: 0, message: "No status record available yet." });
      }else{
        if(count){
          _data = _data.splice(0, count);
        }
        var b = _data;
        if(req.body.onlyModified == true){
          b = [];
          for(var i in _data){
            if(_data[i].modified.length === 0){
              delete _data[i].modified;
              b.push(_data[i]);
            }else{
              var c = {};
              for(var j in _data[i].modified){
                c[_data[i].modified[j]] = _data[i][_data[i].modified[j]];
              }
              b.push(c);
            };
          }
        }
        // if(key_to_fetch){
        //   var _newArr = [];
        //   _.map(b, function(v){
        //     if(v[key_to_fetch]){
        //       var _objOfB = {
        //         timestamp: v.timestamp
        //       };
        //       _objOfB[key_to_fetch] = v[key_to_fetch];
        //       _newArr.push(_objOfB);
        //     }
        //   });
        //   b = _newArr;
        // }
        res.send({status: 1, message: "Required states are retrieved successfully.", data: b });
      }
    }
  });
};

Events.prototype.setSmsStatus = function(req, res) {
  var that = this
  , obj = req.body.reqObject;
  obj.hub_id = req.body.hub_id;
  obj.device_b_one_id = req.body.device_b_one_id;
  obj.timestamp = new Date();

  db.save('smsState', obj, function(err, data){
    if(err){
      res.send({status: 0, message: "Something went wrong. Unable to set state. Please contact admin." });
    }else{
      res.send({status: 1, message: "added status successfully.", data: data });
    }
  });
};

Events.prototype.updateSmsStatus = function(req, res) {
  var that = this
  , obj = req.body.reqObject
  , sms = req.body.reqObject.sms;
  hub_id = req.body.hub_id;
  device_b_one_id = req.body.device_b_one_id;
  obj.timestamp = new Date();
  delete obj.sms;

  if(sms){
    var keys = Object.keys(sms);
    for(var i in keys){
      if(typeof sms[keys[i]] == "object"){
        var inKeys = Object.keys(sms[keys[i]]);
        for(var j in inKeys){
          obj["sms."+keys[i]+"."+inKeys[j]] = sms[keys[i]][inKeys[j]];
        }
      }else{
        obj["sms."+keys[i]] = sms[keys[i]];
      }
    }
  }

  db.findSortedObjects('smsState', { hub_id: hub_id, device_b_one_id: device_b_one_id }, {timestamp: -1}, function(err, data){
    if(err){
      res.send({status: 0, message: "Something went wrong. Unable to update state. Please contact admin." });
    }else{
      db.update('smsState', data[0]._id, obj, function(err, _data){
        if(err){
          res.send({status: 0, message: "Something went wrong. Unable to update state. Please contact admin." });
        }else{
          res.send({status: 1, message: "Status updated successfully.", data: _data });
        }
      });
    }
  });
};

Events.prototype.getSmsStatus = function(req, res) {
  var that = this
  , query = {};
  query.hub_id = req.body.hub_id
  if(req.body.device_b_one_id){
    query.device_b_one_id = req.body.device_b_one_id;
  }

  db.findSortedObjects('smsState', query, {timestamp: -1}, function(err, data){
    if(err){
      res.send({status: 0, message: "Something went wrong. Unable to get state. Please contact admin." });
    }else{
      if(req.body.count){
        data = data.splice(0, req.body.count);
      }
      res.send({status: 1, message: "Status retrieved successfully.", data: data });
    }
  });
};

Events.prototype.getEventsCount = function(req, res) {
  var query = { hub_id: req.body.hub_id, event_status : "Created" };
  if(req.body.device_b_one_id){
    query.device_b_one_id = req.body.device_b_one_id;
  }
  db.findSortedObjects('events', query, {timestamp: 1}, function(err, data){
    if(err){
      res.send({status: 0, message: "Something went wrong. Please contact admin."});
    }else{
      res.send({status: 1, message: "Data retrieved successfully.", count: data.length});
    }
  });
};

Events.prototype.beseye=function(req,res)
{
      var that =this;
  //logger.info("data comming : : "+ JSON.stringify(req.body));
  var resdata=_.pluck(req.body.data,"trigger_identity")
//  logger.info(resdata);
  this.beseyeTriggerApi(resdata);
  res.send({status: 1, message: "Data received successfully."});


};

Events.prototype.besStatus=function(req,res)
{
var that=this;
   logger.info("data receiving"+JSON.stringify(req.body));
  var dataset=req.body;
    logger.info("data receiving from hub for bes status "+JSON.stringify(dataset));
   var hub_id=dataset.hub_id;
  var bone_device_id=dataset.device_b_one_id;
  var query={hub_id:hub_id,device_b_one_id:"0000"};

  db.findSortedObjects("states", query, {timestamp: -1}, function(err, data){

            if(err)
            {
             //  logger.error("latest data :: "+ Json.stringify(data));
             logger.error("Some thing went wrong in in retrivin  the beseye status");
            }
            else {
                    logger.error("latest data of  0000  for bes status update :: "+ JSON.stringify(data));
                    var url;
                     if(data.length >0)
                     {
                       if(req.body.reqObject.cam_status==="1")
                       {
                         url='https://oregon-p1-stage-api-1.beseye.com/open_api/v1/actions/set_cam_status_on'
                        that.callbeseyeStatus(url,dataset,data);
                       }
                       else
                       {
                         url='https://oregon-p1-stage-api-1.beseye.com/open_api/v1/actions/set_cam_status_off'
                        that.callbeseyeStatus(url,dataset,data);
                       }

                     }

            }

    },1);


  res.send({status: 1, message: "Data received successfully."});
};

Events.prototype.callbeseyeStatus=function(url,reqdata,datares)
{
  var that=this;
   var jsondata={
     "actionFields":{"vcam_id": reqdata.reqObject.cam_id}
   }
   logger.info("json data for bes status "+JSON.stringify(jsondata));
  var options = {
                           url: url,
                           method: 'POST',
                           port:443,
                           headers: {
                             'Content-Type': 'application/json',
                             'Authorization':'Bearer '+datares[0].beseye_access_token,
                             'Accept':'application/json',
                             'Accept-charset':'utf-8'
                           },
                           json:jsondata
                    };

 request(options, function(err, res, resbody){


        if(res.statusCode === 401)
        {
             logger.info("access token changed for beseye status ");
             logger.error("responce  code comming here is"+res.statusCode );
             logger.error("cid"+Globals.beseyeDevClientId);
             logger.error("csct : "+Globals.beseyeDevClientSecret);
             logger.error("rf token "+ datares[0].beseye_refresh_token);

            var options = { method: 'POST',
               url: 'https://oregon-p1-stage-api-1.beseye.com/oauth/oauth/token',
               headers:
                {

                  'content-type': 'application/x-www-form-urlencoded' },
               form:
                { grant_type: 'refresh_token',
                  client_id: Globals.beseyeDevClientId,
                  client_secret: Globals.beseyeDevClientSecret,
                  refresh_token: datares[0].beseye_refresh_token } };

             request(options, function (error, response, rsbody) {
               if (error)
               {
                 console.log(rsbody);
               }
               else {

                 logger.info("new access_token"+JSON.stringify(rsbody));
                 var dd=JSON.parse(rsbody);

                 var req ={
                                 hub_id:datares[0].hub_id,
                                 device_id:datares[0].device_id,
                                 device_b_one_id:"0000"

                               };
                 var reqObject={

                                   beseye_access_token:dd.access_token,
                                   beseye_refresh_token:dd.refresh_token,
                                   beseye_expire_in:dd.expires_in
                                };
                 req.reqObject=reqObject;
                 logger.info("json data to set status of 0000 new access token"+JSON.stringify(req));
                 logger.info("info for bes status for new acess token"+JSON.stringify(req));
                 that.besSetStatetwo(req,url,reqdata,datares);

           }
        });


        }
        else {
          logger.info("data upadted sucessfully"+JSON.stringify(resbody));

            var query={hub_id:reqdata.hub_id,device_b_one_id:reqdata.device_b_one_id};

          db.findSortedObjects("states", query, {timestamp: -1}, function(err, data){

              logger.info("data upadted sucessfully"+JSON.stringify(data));

            var req ={
                            hub_id:data[0].hub_id,
                            device_id:data[0].device_id,
                            device_b_one_id:reqdata.device_b_one_id

                          };
            var reqObject={

                              "status":reqdata.reqObject.cam_status
                           };
                           req.reqObject=reqObject;
                           logger.info("json data for besye turn on off "+JSON.stringify(req));

                           that.besSetState(req);



          },1);

          //
          // var req ={
          //                 hub_id:reqdata.hub_id,
          //                 device_id:reqdata.device_id,
          //                 device_b_one_id:reqdata.device_b_one_id
          //
          //               };
          // var reqObject={
          //
          //                   "status":reqdata.reqObject.cam_status
          //                };
          //                req.reqObject=reqObject;
          //                logger.info("json data for besye turn on off "+JSON.stringify(req));
          //
          //                that.besSetState(req);

        }


 });



};

Events.prototype.beseyeTriggerApi=function(resdata)
{

  // getting the latest status  of the devices
 var that=this;
  _.each(resdata,function(v){
           logger.info("dat in bes trigger api"+v);
     var dataSplit=v.split("_");
     var device_id;
     logger.info("dat in datasplit " + dataSplit);

    var query={hub_id:dataSplit[1],device_b_one_id:dataSplit[0]};
    var accessquery={hub_id:dataSplit[1],device_b_one_id:"0000"};

   logger.info("query of the data 0000 " + JSON.stringify(accessquery));
   logger.info("query of the data  b one device id " + JSON.stringify(accessquery));




   db.findSortedObjects("states", accessquery, {timestamp: -1}, function(err, data){

           logger.error("latest data of 0000  for acess token :: "+ JSON.stringify(data));
             if(err)
             {

              logger.error("Some thing went wrong in in retriving  the beseye status");
             }
             else {
                     //logger.error("latest data :: "+ JSON.stringify(data));


                      db.findSortedObjects("states", query, {timestamp: -1}, function(err, devicedata){

                        device_id=devicedata[0].device_id;
                         logger.info("devce id "+device_id);
                         if(devicedata.length >0)
                         {

                            if(dataSplit[2]==="motion")
                            {
                              var url= 'https://oregon-p1-stage-api-1.beseye.com/open_api/v1/triggers/detect_motion_event';
                               that. callTrigger(url,"Motion",data,v,dataSplit[3],device_id);
                            }
                            else {
                              var url= 'https://oregon-p1-stage-api-1.beseye.com/open_api/v1/triggers/detect_human_event';
                               that. callTrigger(url,"Human",data,v,dataSplit[3],device_id);
                            }


                         }





                        },1);

             }

     },1);


  });


};



Events.prototype.besSendEvent=function(req)
{

  var that = this
  , hub_id = req.hub_id
  , device_b_one_id = req.device_b_one_id
  , device_id=req.device_id;

  if(req.reqObject && typeof(req.reqObject) == "string"){
    req.reqObject = JSON.parse(req.reqObject);
  }
  //res.send({status: 0, message: "No such B.One device connected to hub."});


    QueueHandler.eventQueue({
      hub_id: hub_id, // id of the hub
      device_b_one_id: device_b_one_id, // unique B.One device id
      origin_id: 0,
      device_id: device_id,
      eventObj: req.evtObject || {},
      timestamp: new Date(),
      event_status: "Created"
    });
  logger.info("Message is pushig to queue.");
   that.sendbesNotification(req);


};

Events.prototype.besSetState=function(req)
{
  var that=this
  , obj=req.reqObject || {};
  var data_delete_id;// variable for storing the  ids  to be removed
  var delete_ids=[];// array declaration
  var modified = Object.keys(obj);
  obj.hub_id = req.hub_id;
  obj.device_b_one_id = req.device_b_one_id;
  obj.timestamp = new Date();
  obj.modified_by = 0; // origin_id = 0 means hub else app
  obj.device_id = req.device_id;
  dicId=req.device_id;
  modified.push("timestamp");
//  that.validateHubDevice(obj.hub_id, obj.device_b_one_id,function(dicId){

    db.findSortedObjects('states', {device_id: obj.device_id}, {timestamp: -1}, function(err, data){
      if(err){
        logger.error( "Something went wrong. State is not updated. Please contact admin." );
      }else if(!data.length){
        obj.modified = [];
        obj.device_record_count = 1;
        that.bessaveToHistory(obj);
      }else{
        data = data[0];
        delete data._id;
        delete obj._id;
        data.modified = modified;
        _.each(obj, function(v, k) { data[k] = v; });
        if (data.device_record_count) {
          that.besInsertState(data);
        }else{
          db.findCountByQuery('states', {device_id: device_id}, function(err, count){
            if(err){
              logger.error( "Something went wrong. State is not updated. Please contact admin." );
              return false;
            }
            data.device_record_count = count;
            that.besInsertState(data);
          });
        }
      }
    }, 1);
 // });


};


Events.prototype.besSetStateone=function(req,type)
{
  var that=this
  , obj=req.reqObject || {};
  var data_delete_id;// variable for storing the  ids  to be removed
  var delete_ids=[];// array declaration
  var modified = Object.keys(obj);
  obj.hub_id = req.hub_id;
  obj.device_b_one_id = req.device_b_one_id;
  obj.timestamp = new Date();
  obj.modified_by = 0; // origin_id = 0 means hub else app
  obj.device_id = req.device_id;
  dicId=req.device_id;
  modified.push("timestamp");
//  that.validateHubDevice(obj.hub_id, obj.device_b_one_id,function(dicId){

    db.findSortedObjects('states', {device_id: obj.device_id}, {timestamp: -1}, function(err, data){
      if(err){
        logger.error( "Something went wrong. State is not updated. Please contact admin." );
      }else if(!data.length){
        obj.modified = [];
        obj.device_record_count = 1;
        that.bessaveToHistoryone(obj,type);
      }else{
        data = data[0];
        delete data._id;
        delete obj._id;
        data.modified = modified;
        _.each(obj, function(v, k) { data[k] = v; });
        if (data.device_record_count) {
          that.besInsertStateone(data,type);
        }else{
          db.findCountByQuery('states', {device_id: device_id}, function(err, count){
            if(err){
              logger.error( "Something went wrong. State is not updated. Please contact admin." );
              return false;
            }
            data.device_record_count = count;
            that.besInsertStateone(data,type);
          });
        }
      }
    }, 1);
 // });


};



Events.prototype.besSetStatetwo=function(req,url,reqdata,datares)
{
  var that=this
  , obj=req.reqObject || {};
  var data_delete_id;// variable for storing the  ids  to be removed
  var delete_ids=[];// array declaration
  var modified = Object.keys(obj);
  obj.hub_id = req.hub_id;
  obj.device_b_one_id = req.device_b_one_id;
  obj.timestamp = new Date();
  obj.modified_by = 0; // origin_id = 0 means hub else app
  obj.device_id = req.device_id;
  dicId=req.device_id;
  modified.push("timestamp");
//  that.validateHubDevice(obj.hub_id, obj.device_b_one_id,function(dicId){

    db.findSortedObjects('states', {device_id: obj.device_id}, {timestamp: -1}, function(err, data){
      if(err){
        logger.error( "Something went wrong. State is not updated. Please contact admin." );
      }else if(!data.length){
        obj.modified = [];
        obj.device_record_count = 1;
        that.bessaveToHistorytwo(obj,url,reqdata,datares);
      }else{
        data = data[0];
        delete data._id;
        delete obj._id;
        data.modified = modified;
        _.each(obj, function(v, k) { data[k] = v; });
        if (data.device_record_count) {
          that.besInsertStatetwo(data,url,reqdata,datares);
        }else{
          db.findCountByQuery('states', {device_id: device_id}, function(err, count){
            if(err){
              logger.error( "Something went wrong. State is not updated. Please contact admin." );
              return false;
            }
            data.device_record_count = count;
            that.besInsertStatetwo(data,url,reqdata,datares);
          });
        }
      }
    }, 1);
 // });


};


Events.prototype.besInsertState=function(obj)
{
  var that = this;
  if(obj.device_record_count >= Globals.maxMongoRecordCount){
    db.findFieldsByObjects('states', {device_id: obj.device_id}, { _id: 1 }, function(err, data){
      if(err){
      logger.error( "Something went wrong. State is not updated. Please contact admin." );
        return false;
      }
      data = _.map(function(doc) { return doc._id; });
      db.removeManyByObject("states", data, function(err, resData){
        if(err){
          logger.error( "Something went wrong. records are not deleted. Please contact admin." );
        }else{
          obj.device_record_count = (Globals.resetMongoRecordCount + 1);
          that.bessaveToHistory(obj);
        }
      });
    }, (Globals.maxMongoRecordCount - Globals.resetMongoRecordCount));
  }else{
    obj.device_record_count = (obj.device_record_count + 1);
    that.bessaveToHistory( obj);
  }

};

Events.prototype.besInsertStateone=function(obj,type)
{
  var that = this;
  if(obj.device_record_count >= Globals.maxMongoRecordCount){
    db.findFieldsByObjects('states', {device_id: obj.device_id}, { _id: 1 }, function(err, data){
      if(err){
      logger.error( "Something went wrong. State is not updated. Please contact admin." );
        return false;
      }
      data = _.map(function(doc) { return doc._id; });
      db.removeManyByObject("states", data, function(err, resData){
        if(err){
          logger.error( "Something went wrong. records are not deleted. Please contact admin." );
        }else{
          obj.device_record_count = (Globals.resetMongoRecordCount + 1);
          that.bessaveToHistoryone(obj,type);
        }
      });
    }, (Globals.maxMongoRecordCount - Globals.resetMongoRecordCount));
  }else{
    obj.device_record_count = (obj.device_record_count + 1);
    that.bessaveToHistoryone( obj,type);
  }

};


Events.prototype.besInsertStatetwo=function(obj,url,reqdata,datares)
{
  var that = this;
  if(obj.device_record_count >= Globals.maxMongoRecordCount){
    db.findFieldsByObjects('states', {device_id: obj.device_id}, { _id: 1 }, function(err, data){
      if(err){
      logger.error( "Something went wrong. State is not updated. Please contact admin." );
        return false;
      }
      data = _.map(function(doc) { return doc._id; });
      db.removeManyByObject("states", data, function(err, resData){
        if(err){
          logger.error( "Something went wrong. records are not deleted. Please contact admin." );
        }else{
          obj.device_record_count = (Globals.resetMongoRecordCount + 1);
          that.bessaveToHistorytwo(obj,url,reqdata,datares);
        }
      });
    }, (Globals.maxMongoRecordCount - Globals.resetMongoRecordCount));
  }else{
    obj.device_record_count = (obj.device_record_count + 1);
    that.bessaveToHistorytwo( obj,url,reqdata,datares);
  }

};




Events.prototype.bessaveToHistory=function(datatosave){


  _collectionName =  "states";

    db.save(_collectionName, datatosave, function(err, hisData){
      if(err){
        logger.error( "Something went wrong. State is not updated. Please contact admin." );
      }else{
        logger.error( "State is updated successfully.");


      }
    });

};


Events.prototype.bessaveToHistoryone=function(datatosave,type){

   var that=this;
  _collectionName =  "states";

    db.save(_collectionName, datatosave, function(err, hisData){
      if(err){
        logger.error( "Something went wrong. State is not updated. Please contact admin." );
      }else{

        logger.error( "State is new refresh token save  successfully to 0000 "+ JSON.stringify(datatosave));
          logger.error( "recalling bestrigger api in ac token failure case "+ type);




          var query={hub_id:datatosave.hub_id,device_b_one_id:"0000"};

          db.findSortedObjects("states", query, {timestamp: -1}, function(err, data)
          {

             logger.error( "State is refresh_token updated successfully new data res."+ JSON.stringify(data))
                   //return ;
                   //that.callbeseyeStatus(url,reqdata,data);
                var tid= [];
                tid[0]=data[0].beseye_trigger_id;
                logger.error( "retrived tid from the database in fail act case "+ tid);
                that.beseyeTriggerApi(tid);

          },1);



      }
    });

};


Events.prototype.bessaveToHistorytwo=function(datatosave,url,reqdata,datares){

   var that=this;
  _collectionName =  "states";

    db.save(_collectionName, datatosave, function(err, hisData){
      if(err){
        logger.error( "Something went wrong. State is not updated. Please contact admin." );
      }else{
        logger.error( "State is updated successfully.");


        logger.error( "State is refresh_token updated successfullyand new data."+ JSON.stringify(datatosave));
       logger.error( "State is refresh_token updated successfully old data res."+ JSON.stringify(datares));


       var query={hub_id:reqdata.hub_id,device_b_one_id:"0000"};

       db.findSortedObjects("states", query, {timestamp: -1}, function(err, data){

          logger.error( "State is refresh_token updated successfully new data res."+ JSON.stringify(data));
          logger.error( "malik sir data res."+ JSON.stringify(reqdata));
                //return ;
                that.callbeseyeStatus(url,reqdata,data);

       },1);


      }
    });

};





Events.prototype.callTrigger=function(url,triType,devdata,tid,cam_id,device_id)
{

  logger.info("json data while comming to callTrigger:"+JSON.stringify(devdata));
  var that=this;
  var bone_split=tid.split("_");
    logger.error("trigger id  ::"+tid);
    logger.error("act device id ::"+bone_split);
   var jsondata={ "trigger_identity": tid,
                 "triggerFields": {"vcam_id": cam_id}
                }

  logger.info("json data formed:"+JSON.stringify(jsondata));
  var options = {
                           url: url,
                           method: 'POST',
                           port:443,
                           headers: {
                             'Content-Type': 'application/json',
                             'Authorization':'Bearer '+devdata[0].beseye_access_token,
                             'Accept':'application/json',
                             'Accept-cherset':'utf-8'
                           },
                           json:jsondata
                    };
                    request(options, function(err, res, resbody){

                                                 if(res.statusCode == 401){

                                                    logger.error("responce  code comming here is"+res.statusCode );
                                                    logger.error("cid"+Globals.beseyeDevClientId);
                                                    logger.error("csct : "+Globals.beseyeDevClientSecret);
                                                    logger.error("rf token "+ devdata[0].beseye_refresh_token);

                                                   var options = { method: 'POST',
                                                      url: 'https://oregon-p1-stage-api-1.beseye.com/oauth/oauth/token',
                                                      headers:
                                                       {

                                                         'content-type': 'application/x-www-form-urlencoded' },
                                                      form:
                                                       { grant_type: 'refresh_token',
                                                         client_id: Globals.beseyeDevClientId,
                                                         client_secret: Globals.beseyeDevClientSecret,
                                                         refresh_token: devdata[0].beseye_refresh_token } };

                                                    request(options, function (error, response, rsbody) {
                                                      if (error)
                                                      {
                                                        console.log(rsbody);
                                                      }
                                                      else {

                                                        logger.info("new access_token"+JSON.stringify(rsbody));
                                                        var dd=JSON.parse(rsbody);

                                                        var req ={
                                                                        hub_id:devdata[0].hub_id,
                                                                        device_id:devdata[0].device_id,
                                                                        device_b_one_id:"0000"


                                                                      };
                                                        var reqObject={

                                                                          beseye_access_token:dd.access_token,
                                                                          beseye_refresh_token:dd.refresh_token,
                                                                          beseye_expire_in:dd.expires_in,
                                                                          beseye_trigger_id:tid
                                                                       };
                                                        req.reqObject=reqObject;
                                                        logger.info("json data to set status of 0000 new access token"+JSON.stringify(req));
                                                        logger.info("need to do the setstatus of new access token");
                                                        that.besSetStateone(req,tid);



                                                      }


                                                    });


                                                 }
                                                 else
                                                 {
                                                      //logger.error("responce code"+res.statusCode );
                                                      var msgoftrigger;
                                                      logger.info("sucess in  event ::"+JSON.stringify(resbody));
                                                      if(triType==="Human")
                                                      {
                                                         msgoftrigger="human";
                                                      }
                                                      else {
                                                        msgoftrigger="motion";
                                                      }

                                                       if(resbody.data.length>0)
                                                       {
                                                          var tritime=resbody.data[0].meta.timestamp;
                                                          logger.error("getted time is"+tritime);
                                                          var req ={
                                                                          hub_id:devdata[0].hub_id,
                                                                          device_id:device_id,
                                                                          device_b_one_id:bone_split[0],
                                                                          message:triType
                                                                        };
                                                          var reqObject={

                                                                            latest_thumbnail_timestamp:tritime,
                                                                            last_event_trigger_type:msgoftrigger
                                                                         };

                                                                         if(triType==="Human")
                                                                                   {
                                                                                     var evntreqObject={
                                                                                       "beseye_trigger":"0"
                                                                                     }
                                                                                   }
                                                                                   else {
                                                                                     var evntreqObject={
                                                                                       "beseye_trigger":"1"
                                                                                     }
                                                                                   }      req.evtObject=evntreqObject;
                                                                                          req.reqObject=reqObject;

                                                                                          logger.info("json data to set status of deviceid if sucess"+JSON.stringify(req));
                                                                                            that.besSendEvent(req);

                                                       }

                                                    //  var dd=JSON.parse(resbody);
                                                    //  if(dd.data.length>0)
                                                    //  {
                                                      //     _.each(dd.data,function(v){
                                                      //          if(v.vcam_id===devdata[0].vcam_id)
                                                      //          {
                                                      //            var req ={
                                                      //                 hub_id:devdata[0].hub_id,
                                                      //                 device_id:devdata[0].device_id,
                                                      //                 device_b_one_id:devdata[0].device_b_one_id
                                                      //               };
                                                      //             var reqObject={
                                                      //                    latest_thumbnail_url:v.latest_thumbnail_url,
                                                      //                    latest_thumbnail_timestamp:v.latest_thumbnail_timestamp,
                                                      //                    status:v.status,
                                                      //                    last_event_trigger_type:triType
                                                      //
                                                      //
                                                      //                  }
                                                      //
                                                      //
                                                      //           if(triType==="human")
                                                      //           {
                                                      //             var evntreqObject={
                                                      //               "beseye_trigger":"0"
                                                      //             }
                                                      //           }
                                                      //           else {
                                                      //             var evntreqObject={
                                                      //               "beseye_trigger":"1"
                                                      //             }
                                                      //           }      req.evtObject=evntreqObject;
                                                      //                  req.reqObject=reqObject;
                                                      //                    that.besSendEvent(req);
                                                      //                  //that.besSetState(req);
                                                      // //             logger.info("data for send event"+JSON.stringify(req));
                                                      //          }
                                                      //     });
                                                    //  }
                                                 }
                                           });





};




Events.prototype.getEnergyValues=function(req,res)
{
  var that = this
  , hub_id = req.body.hub_id
  , device_b_one_id = req.body.device_b_one_id
  , billDate=req.body.billDate
  , count = req.body.count
  , key_to_fetch = req.body.key_to_fetch;

  var dateArray = [billDate, this.getDateString() ];

  this.getEnergyStatus('states', res, count, 0, dateArray, hub_id, device_b_one_id, key_to_fetch);
};

Events.prototype.getDateString = function(_date){
  var date = _date || new Date(), day = _date ? 0 : 1;
  return date.getFullYear() + "-" + ("00" + (date.getMonth() + 1)).slice(-2) + "-" + ("00" + (date.getDate() + day)).slice(-2);
};

Events.prototype.getEnergyStatus = function(_collectionName, res, count, onlyModified,arraydata,hub,device, key_to_fetch) {
  var that = this;
  var qry={hub_id:hub,timestamp:{"$gte": new Date(arraydata[0]),"$lt":new Date(arraydata[1])},"device_b_one_id": device};
  db.findSortedObjects(_collectionName, qry, {timestamp: -1}, function(err, data){
    if(err){
      res.send({status: 0, message: "Something went wrong. Unable to retrive state. Please contact admin." });
    }else{
      if(data.length === 0){
        res.send({status: 0, message: "No status record available yet." });
      }else{
        var _obj = {};
        _.each(data, function(v){
          var key = that.getDateString(v.timestamp);
          var key_to_fetch_arr = key_to_fetch.split(",");
          // if(key_to_fetch_arr.length == 1 && v.hasOwnProperty(key_to_fetch)){ // && !_obj[key]){ // for getting the last data of evch date.
            _obj[key] = {
              timestamp: Utils.getTimeStamp(v.timestamp)
              // fetched_key_value: v[key_to_fetch]
            };
            _.each(key_to_fetch_arr, function(_v){
              _obj[key][_v] = v[_v];
            });
          // }
        });
        data =  _.map(_obj, function(v, k){ return v; });
        data = data.reverse();
        if(count){
          data = data.splice(0, count);
        }
        res.send({status: 1, count: data.length, data: data });
      }
    }
  });
};

Events.prototype.fetchHubOnOff = function(req, res){
  var query = {}, that = this;

  if(req.body.hub_id){
    query.hub_id = {$in: req.body.hub_id.split(",")};
  }
  db.findSortedObjects('hub_on_off_status', query, {timestamp: -1}, function(err, data){
    if(err){
      res.send({status: 0, message: "Unable to fetch the data. Please try again."});
    }else if(data[0]){
      var _obj = {};
      _.each(data, function(v){
        _obj[v.hub_id] = _obj[v.hub_id] || [];
        v.timestamp = Utils.getTimeStamp(v.timestamp);
        _obj[v.hub_id].push(v);
      });
      res.send({status: 1, message: "Fetched Data successfully.", data: _obj});
    }else{
      res.send({status: 0, message: "No Data available."});
    }
  });
}

Events.prototype.ninjaLock=function(req,res)
{
  var api_token=req.body.api_token;
  var serial=req.body.serial;
  //logger.info("values "+ api_token + "setri" + serial);
  var Url_hit="https://api.ninjalock.me/livesmart/get_sv_seed";

  //res.redirect(Url_hit+'?api_token=' + api_token + '&serial=' + serial );
  request(Url_hit+'?api_token=' + api_token + '&serial=' + serial , function(error, response, body) {
  console.log(body);
  res.send(body);
   });

}

Events.prototype.ninjaDoLock=function(req,res)
{
  var api_token=req.body.api_token;
  var serial=req.body.serial;
  var tid=req.body.tid;
  var sv_seed=req.body.sv_seed;
  var bt_seed=req.body.bt_seed;
  //logger.info("values "+ api_token + "setri" + serial);
  var Url_hit="https://api.ninjalock.me/livesmart/get_lock_command";

  //res.redirect(Url_hit+'?api_token=' + api_token + '&serial=' + serial );
  request(Url_hit+'?api_token=' + api_token + '&serial=' + serial +'&tid='+tid +'&sv_seed='+sv_seed+'&bt_seed='+bt_seed, function(error, response, body) {
  console.log(body);
  res.send(body);
   });

}

Events.prototype.ninjaDoUnlock=function(req,res)
{
  var api_token=req.body.api_token;
  var serial=req.body.serial;
  var tid=req.body.tid;
  var sv_seed=req.body.sv_seed;
  var bt_seed=req.body.bt_seed;
  //logger.info("values "+ api_token + "setri" + serial);
  var Url_hit="https://api.ninjalock.me/livesmart/get_unlock_command";

  //res.redirect(Url_hit+'?api_token=' + api_token + '&serial=' + serial );
  request(Url_hit+'?api_token=' + api_token + '&serial=' + serial +'&tid='+tid +'&sv_seed='+sv_seed+'&bt_seed='+bt_seed , function(error, response, body) {
  console.log(body);
  res.send(body);
   });

}


Events.prototype.ninjaStatus=function(req,res)
{
  var api_token=req.body.api_token;
  var serial=req.body.serial;
  var tid=req.body.tid;
  var sv_seed=req.body.sv_seed;
  var bt_seed=req.body.bt_seed;
  //logger.info("values "+ api_token + "setri" + serial);
  var Url_hit="https://api.ninjalock.me/livesmart/get_status_command";

  //res.redirect(Url_hit+'?api_token=' + api_token + '&serial=' + serial );
  request(Url_hit+'?api_token=' + api_token + '&serial=' + serial +'&tid='+tid +'&sv_seed='+sv_seed+'&bt_seed='+bt_seed , function(error, response, body) {
  console.log(body);
  res.send(body);
   });

}

Events.prototype.ninjaResult=function(req,res)
{
  var api_token=req.body.api_token;
  var serial=req.body.serial;
  var response=req.body.response;
  var sv_seed=req.body.sv_seed;
  var bt_seed=req.body.bt_seed;
  //logger.info("values "+ api_token + "setri" + serial);
  var Url_hit="https://api.ninjalock.me/livesmart/get_command_result";

  //res.redirect(Url_hit+'?api_token=' + api_token + '&serial=' + serial );
  request(Url_hit+'?api_token=' + api_token + '&serial=' + serial +'&response='+response +'&sv_seed='+sv_seed+'&bt_seed='+bt_seed , function(error, response, body) {
  console.log(body);
  res.send(body);
   });

}


Events.prototype.getModels = function(){
  if(this.deviceModel){
    return ;
  }
  var Models = mysqlDb.getModels();
  this.deviceModel = Models.Devices;
  this.deviceCategoryModel = Models.DeviceCategory;
  this.hubRoomModel = Models.HubRoom;
  this.userDevModel = Models.UserDev;
  this.userHubModel = Models.UserHub;
  this.hubDefModel = Models.HubDef;
  this.roomModel = Models.Room;
  this.hubModel = Models.Hub;
  this.lineModel=Models.LineUser;
};

module.exports = new Events();
