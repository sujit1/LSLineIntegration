var express = require('express')
    , router = express.Router()
    , _ = require('underscore')
    , db = require('../../mySqlDB')
    , Globals = require('../../globals')
    , Sequelize = require('sequelize')
    , User = require('../User/Users')
    , Utils = require('../Commons/Utils')
    , log4js = require('log4js');

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

function Device() {
    _.bindAll(this, "addDevice", "getDevices", "updateDevice", "deleteDevice", "getDevicesInRoom", "getModels", "setRemoteKey", "getRemoteKeys", "updateRemoteKey", "updateDeviceOnId");

    router.post('/addDevice', User.validateSession, this.addDevice);
    router.post('/getDevices', User.validateSession,this.getDevices);
    router.post('/updateDevice', User.validateSession, this.updateDevice);
    router.post('/deleteDevice', User.validateSession, this.deleteDevice);
    router.post('/getDevicesInRoom', User.validateSession, this.getDevicesInRoom);
    router.post('/setRemoteKey', User.validateSession, this.setRemoteKey);
    router.post('/getRemoteKeys', User.validateSession, this.getRemoteKeys);
    // router.post('/updateRemoteKey', User.validateSession, this.updateRemoteKey);

    return router;
};

Device.prototype.addDevice = function(req, res) {
    var that = this
    , hub_id = req.body.hub_id
    , room_id = req.body.room_id
    , room_name = req.body.room_name
    , obj = {};

    obj.category_id = req.body.category_id;
    obj.device_name = req.body.device_name;
    obj.key_count = req.body.key_count;
    obj.protocol = req.body.protocol;
    obj.device_b_one_id = req.body.device_b_one_id;

    that.getModels();
    that.deviceCategoryModel.findAll({where: {category_id: obj.category_id}})
        .then(function(devCat){
            if(devCat.length === 0){
                res.send({status: 0, message: "Device category does not exist. Please contact admin to add device category."});
            }else{
                if(!obj.device_name){
                    obj.device_name = devCat[0].dataValues.category_name;
                }
                that.hubRoomModel.findAll({where: {hub_id: hub_id}})
                    .then(function(hubRoomRec){
                        var val;
                        if(Utils.isOriginHub(req.body.origin_id)){
                            var rooms = _.chain(hubRoomRec)
                                        .pluck("room_name")
                                        .uniq()
                                        .value();
                            val = rooms.indexOf(room_name);
                        }
                        var rooms = _.chain(hubRoomRec)
                                    .pluck("room_id")
                                    .uniq()
                                    .value();
                        if(!Utils.isOriginHub(req.body.origin_id)){
                            val = rooms.indexOf(room_id);
                        }
                        room_id = rooms[val];
                        if(val === -1){
                            res.send({status: 0, message: "No hub is linked with the given room."});
                        }else{
                            obj.room_id = room_id;
                            that.deviceModel.findAll({where: {room_id: {$in: rooms}, device_b_one_id: req.body.device_b_one_id}})
                                .then(function(devs){
                                    if(devs.length === 0){
                                        that.deviceModel.build(obj).save()
                                            .then(function(devRec){
                                                res.send({status: 1, message: "Device added successfully.", device_id: devRec.device_id });
                                            });
                                    }else{
                                        res.send({status: 0, message: "The device with the given B.One id already exist."});
                                    }
                                });
                        }
                    })
                    .catch(function(err){
                        res.send({status: 0, message: err.message });
                    });
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message });
        });
};

Device.prototype.getDevices = function(req, res) {
    var that = this
    , hub_id = req.body.hub_id
    , category_type = req.body.category_type
    that.getModels();
    that.hubRoomModel.findAll({attributes: ['room_id'], where: {hub_id: hub_id}})
        .then(function(deviceIds){
            if(deviceIds.length === 0){
                res.send({status: 0, message: "No Devices are linked to this hub."});
            }else{
                var arr = _.pluck(deviceIds, "room_id");
                that.deviceModel.findAll({where: {$and: {room_id: { $in: arr }, device_state: "ADDED"}}})
                    .then(function(devices){
                        var categoryArr = _.pluck(devices, "category_id");
                        var query = {category_id: {$in : categoryArr }};
                        if(category_type){
                            query.category_type = category_type;
                        }
                        that.deviceCategoryModel.findAll({attributes: ['category_id', 'category_type', 'category_name', 'device_radio_type'], where: { $and: query}})
                            .then(function(category){
                                var reqCategories = _.pluck(category, 'category_id');
                                var reqDevices = _.chain(devices)
                                                    .map(function(dev){ if(reqCategories.indexOf(dev.category_id) !== -1){
                                                        dev.dataValues.category_type = category[reqCategories.indexOf(dev.category_id)].category_type ;
                                                        dev.dataValues.category_name = category[reqCategories.indexOf(dev.category_id)].category_name ;
                                                        dev.dataValues.device_radio_type = category[reqCategories.indexOf(dev.category_id)].device_radio_type ;
                                                        return dev.dataValues;
                                                    }})
                                                    .compact()
                                                    .sortBy('device_b_one_id')
                                                    .value();

                                var roomIds = _.pluck(reqDevices, 'room_id');
                                that.roomModel.findAll({attributes: ['room_id', 'room_name'], where: {room_id: {$in: roomIds }}})
                                    .then(function(rooms){
                                        _.chain(rooms)
                                            .map(function(roomRec){
                                                var arr = roomIds.reduce(function(a, e, i) {
                                                                if (e === roomRec.dataValues.room_id)
                                                                    a.push(i);
                                                                return a;
                                                            }, []);
                                                _.map(arr, function(v, k){ reqDevices[v]['room_name'] = roomRec.dataValues.room_name; });
                                            })
                                        res.send({status: 1, message: "Got the devices of the given category type in the given hub.", devices: reqDevices });
                                    });
                            });
                    });
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message });
        });
};

Device.prototype.getDevicesInRoom = function(req, res) {
    var that = this
    , room_id = req.body.reqObject.room_id

    that.getModels();
    // that.hubRoomModel.findAll({where: {hub_id: req.body.hub_id}})
    //     .then(function(hubRooms){
    //         if(hubRooms.length === 0){
    //             res.send({status: 0, message: "Hub has no devices connected." });
    //         }else{
    //             var rooms = _.pluck(hubRooms, "room_id");
    //             // that.deviceModel.findAll({where: {room_id: {$in: rooms}, device_b_one_id: req.body.device_b_one_id}})
    //             that.deviceModel.findAll({where: {room_id: {$in: rooms}}})
    //                 .then(function(devices){
    //                     that.updateDeviceOnId(res, obj, devices);
    //                 });
    //         }
    //     })
    //     .catch(function(err){
    //         res.send({status: 0, message: err.message });
    //     });
    that.deviceModel.findAll({where: {$and: {room_id: room_id, device_state: "ADDED"}}})
        .then(function(devices){
            var reqDevices = _.chain(devices)
                                .map(function(dev){
                                    return dev.dataValues;
                                })
                                .compact()
                                .sortBy('device_b_one_id')
                                .value();
            that.roomModel.findAll({attributes: ['room_id', 'room_name'], where: {room_id: room_id}})
                .then(function(room){
                    room = room[0].dataValues;
                    reqDevices.forEach(function(reqDev){
                        reqDev['room_name'] = room.room_name;
                    });
                    res.send({status: 1, message: "Got the devices of the given category type in the given hub and room.", resObject: { devices: reqDevices } });
                });
        })
        .catch(function(err){
            res.send({status: 0, message: err.message });
        });
};

Device.prototype.updateDevice = function(req, res) {
    var that = this
    , obj = {};

    if(req.body.reqObject.hasOwnProperty("security_mode") && req.body.reqObject.security_mode !== ""){
        obj.security_mode = req.body.reqObject.security_mode
    }
    if(req.body.reqObject.hasOwnProperty("notify_me") && req.body.reqObject.notify_me !== ""){
        obj.notify_me = req.body.reqObject.notify_me;
    }

    that.getModels();
    if(Utils.isOriginHub(req.body.origin_id)){
        that.hubRoomModel.findAll({where: {hub_id: req.body.hub_id}})
            .then(function(hubRooms){
                if(hubRooms.length === 0){
                    res.send({status: 0, message: "Hub has no devices connected." });
                }else{
                    var rooms = _.pluck(hubRooms, "room_id");
                    that.deviceModel.findAll({where: {room_id: {$in: rooms}, device_b_one_id: req.body.reqObject.device_b_one_id}})
                        .then(function(devices){
                            that.updateDeviceOnId(res, obj, devices);
                        });
                }
            })
            .catch(function(err){
                res.send({status: 0, message: err.message });
            });
    }else{
        that.deviceModel.findAll({where: {device_id: req.body.reqObject.device_id}})
            .then(function(dev){
                that.updateDeviceOnId(res, obj, dev);
            })
            .catch(function(err){
                res.send({status: 0, message: err.message });
            });
    }

};

Device.prototype.updateDeviceOnId = function(res, obj, dev) {
    var that = this;

    if(dev.length === 0){
        res.send({status: 0, message: "No such device is added."});
    }else{
        that.deviceCategoryModel.findAll({where: {category_id: dev[0].category_id}})
            .then(function(cat){
                if(cat.length === 0){
                    res.send({status: 0, message: "Some internal problem occured. Please contact administrator."});
                }else{
                    if(cat[0].category_type === "Security" && obj.hasOwnProperty("notify_me")){
                        res.send({status: 0, message: "can't change the \"Notify Me\" type for security devices. No action is performed."});
                    }else{
                        that.deviceModel.update(obj, {where: {device_id: dev[0].device_id}})
                            .then(function(){
                                res.send({status: 1, message: "changed the Mode successfully."});
                            });
                    }
                }
            })
            .catch(function(err){
                res.send({status: 0, message: err.message });
            });
    }
};

Device.prototype.deleteDevice = function(req, res) {
    logger.trace('deleteDevice Start: ' + JSON.stringify(req.body));
    var that = this
    , user_id = req.body.user_id
    , device_id = req.body.reqObject.device_id;
    // , password = User.encryptPassword(req.body.password)

    that.getModels();
    // that.userModel.findAll({where: {user_id: user_id, password: password}})
    //     .then(function(user){
    //         if(user.length === 0){
    //             res.send({status: 0, message: "Password entered for confirmation is in-correct."});
    //             return;
    //         }
    that.deviceModel.update({device_deleted_by: user_id, device_state: "DELETED", device_delete_date: Sequelize.NOW}, {where: { device_id: device_id }})
        .then(function(devices){
            if(devices[0] !== 0){
                res.send({status: 1, message: "Device deleted successfully."});
                logger.trace('deleteDevice End');
            }else{
                res.send({status: 0, message: "No Device is present with given id."});
                logger.trace('deleteDevice End');
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message });
        });
        // })
        // .catch(function(err){
        //     res.send({status: 0, message: err.message });
        // });
};

Device.prototype.setRemoteKey = function(req, res) {
    logger.trace("SetRemoteKey start: " + JSON.stringify(req.body));
    var that = this
        , obj = req.body.reqObject;

    obj.device_id = req.body.device_id;
    // obj.key_type = req.body.reqObject.key_type;
    // obj.key_option = req.body.reqObject.key_option;
    // obj.key_state = req.body.reqObject.key_state;
    // obj.key_data = req.body.reqObject.key_data;
    // obj.key_desc = req.body.reqObject.key_desc;
    // obj.key_number = req.body.reqObject.key_number;

    that.getModels();
    that.deviceModel.findAll({where: {device_id: obj.device_id}})
        .then(function(device){
            if(device.length === 0){
                res.send({status: 0, message: "Device doesn't exist. Please contact admin."});
                return ;
            }
            that.remoteKeyInfoModel.findAll({where: {device_id: obj.device_id}})
                .then(function(devKeys){
                    var key = _.pick(devKeys, function(value, key, object) {
                                    return value.dataValues.key_number === obj.key_number;
                                });
                    if(Object.keys(key).length === 0){
                        if(devKeys.length < device[0].key_count){
                            that.remoteKeyInfoModel.build(obj).save()
                                .then(function(data){
                                    res.send({status: 1, message: "Remote Key Information saved successfully."});
                                });
                        }else{
                            res.send({status: 0, message: "Remote Keys count reached max count specified."});
                        }
                    }else{
                        that.remoteKeyInfoModel.update(obj, {where: { key_id: key[Object.keys(key)[0]].dataValues.key_id }})
                            .then(function(devKeys){
                                if(devKeys[0] === 0){
                                    res.send({status: 0, message: "No key is present with the given key and device combination."});
                                }else{
                                    res.send({status: 1, message: "Remote Key Information updated successfully."});
                                }
                            });
                    }
                });
        })
        .catch(function(err){
           res.send({status: 0, message: err.message });
        });
    logger.trace("SetRemoteKey End");
};

Device.prototype.getRemoteKeys = function(req, res) {
    var that = this
        , obj = {};

    if(req.body.reqObject.key_id){
        obj.key_id = req.body.reqObject.key_id;
    }else if(req.body.reqObject.key_number){
        obj.key_number = req.body.reqObject.key_number;
    }

    that.getModels();
    if(Utils.isOriginHub(req.body.origin_id)){
        that.hubRoomModel.findAll({where: {hub_id: req.body.hub_id}})
            .then(function(hubRooms){
                if(hubRooms.length === 0){
                    res.send({status: 0, message: "Hub has no devices connected." });
                }else{
                    var rooms = _.pluck(hubRooms, "room_id");
                    that.deviceModel.findAll({where: {room_id: {$in: rooms}, device_b_one_id: req.body.device_b_one_id}})
                        .then(function(devices){
                            that.getKeysFromDevices(res, obj, devices);
                        });
                }
            })
            .catch(function(err){
                res.send({status: 0, message: err.message });
            });
    }else{
        obj.device_id = req.body.device_id;
        that.deviceModel.findAll({where: {device_id: obj.device_id}})
            .then(function(device){
                that.getKeysFromDevices(res, obj, device);
            })
            .catch(function(err){
               res.send({status: 0, message: err.message });
            });
    }
};

Device.prototype.getKeysFromDevices = function(res, obj, device) {
    var that = this;
    if(device.length === 0){
        res.send({status: 0, message: "Device doesn't exist. Please contact admin."});
        return ;
    }
    obj.device_id = device[0].device_id;

    that.remoteKeyInfoModel.findAll({where: obj})
        .then(function(devKeys){
            if(devKeys.length === 0){
                res.send({status: 0, message: "No Keys are added to Remote."});
            }else{
                res.send({status: 1, message: "Remote Key Information retrieved successfully.", keys: devKeys});
            }
        })
        .catch(function(err){
           res.send({status: 0, message: err.message });
        });
};

Device.prototype.updateRemoteKey = function(req, res) {
    var that = this
        , obj = {}
        , device_id = req.body.device_id
        , key_number = req.body.key_number;

    obj = req.body.reqObject;
    // obj.key_type = req.body.reqObject.key_type;
    // obj.key_option = req.body.reqObject.key_option;
    // obj.key_state = req.body.reqObject.key_state;
    // obj.key_data = req.body.reqObject.key_data;
    // obj.key_desc = req.body.reqObject.key_desc;

    that.getModels();
    that.deviceModel.findAll({where: {device_id: device_id}})
        .then(function(device){
            if(device.length === 0){
                res.send({status: 0, message: "Device doesn't exist. Please contact admin."});
                return ;
            }
            that.remoteKeyInfoModel.update(obj , {where: {device_id: device_id, key_number: key_number}})
                .then(function(devKeys){
                    if(devKeys[0] === 0){
                        res.send({status: 0, message: "No key is present with the given key and device combination."});
                    }else{
                        res.send({status: 1, message: "Remote Key Information updated successfully."});
                    }
                });
        })
        .catch(function(err){
           res.send({status: 0, message: err.message });
        });
};


Device.prototype.getModels = function(){
    if(this.deviceModel){
        return ;
    }
    var Models = db.getModels();
    this.deviceModel = Models.Devices;
    this.deviceCategoryModel = Models.DeviceCategory;
    this.hubRoomModel = Models.HubRoom;
    this.roomModel = Models.Room;
    this.userModel = Models.User;
    this.remoteKeyInfoModel = Models.RemoteKeyInfo;
};

module.exports = new Device();
