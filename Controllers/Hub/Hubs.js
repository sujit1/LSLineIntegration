var express = require('express')
    , router = express.Router()
    , _ = require('underscore')
    , db = require('../../mySqlDB')
    , Globals = require('../../globals')
    , log4js = require('log4js')
    , crypto = require('crypto')
    , User = require('../User/Users')
    , redisDb = require('../../redisDB')
    , Utils = require('../Commons/Utils')
    , mongodb = require('../../db');

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

function Hubs(){
    _.bindAll(this, "addHub", "getHubs", "addHubEmergencyDetails", "getHubContactDetails", "updateHubContactDetails"
        , "connectHub", "addRoomsToHub", "getModels", "addHubDefDetails", "getHubDefDetails", "getHubUsers", "getAllUserDetails", "getDevicesCount", "updateHub"
        , "UpdateHubFullDetails", "addNewHub","generateHubId", "getDummyHubslength","getHubState","getallHubstate","deviceAdd","addNewHublatest");

    // this.getDummyHubslength();
    // router.post('/addHub', User.validateSession, this.addHub);
    router.post('/addNewHub',User.validateSession,this.addNewHub);
    router.post('/addNewHublatest',User.validateSession,this.addNewHublatest);
    router.post('/getHubs', User.validateSession, this.getHubs);
    router.post('/addHubContactDetails', User.validateSession, this.addHubEmergencyDetails);
    router.post('/getHubContactDetails', User.validateSession, this.getHubContactDetails);
    router.post('/updateHubContactDetails', User.validateSession, this.updateHubContactDetails);
    router.post('/addHubDefDetails', User.validateSession, this.addHubDefDetails);
    router.post('/getHubDefDetails', User.validateSession, this.getHubDefDetails);
    router.post('/UpdateHubFullDetails', User.validateSession, this.UpdateHubFullDetails);
    router.post('/connectHub', this.connectHub);
    router.post('/getHubUsers', User.validateSession, this.getHubUsers);
    router.post('/getAllUserDetails',User.validateSession,this.getAllUserDetails);
    router.post('/getDevicesCount', User.validateSession, this.getDevicesCount);
    router.post('/updateHub', User.validateSession, this.updateHub);
    router.post('/addDemoHUbId',this.generateHubId);
    router.post('/getHubState',User.validateSession,this.getHubState);
    router.post('/getallHubstate',this.getallHubstate);
    return router;
};
Hubs.prototype.defaultRooms = ["Living room", "Dining room", "Bedroom", "Hallway", "Kitchen"];

Hubs.prototype.addHub = function(req, res){
    var that = this
    , obj = {}
    , user_id = req.body.user_id
    obj.hub_id = req.body.hub_id;
    obj.model_no = req.body.model_no;
    obj.date_of_mfg = req.body.date_of_mfg;
    obj.software_version = req.body.software_version;
    obj.software_install_date = req.body.software_install_date;

    that.getModels();
    that.hubModel.findAll({where: {hub_id: obj.hub_id}})
        .then(function(hub){
            if(hub.length === 0){
                that.hubModel.build(obj).save()
                    .then(function(newHub){
                        mongodb.save('hub_on_off_status', {hub_id: obj.hub_id, user_id: user_id, status: 1, timestamp: new Date()}, function(){ });
                        that.userHubModel.findAll({where: {user_id: user_id, hub_id: newHub.hub_id}})
                            .then(function(hubs){
                                if(hubs.length === 0){
                                    that.userHubModel.build({user_id: user_id, hub_id: newHub.hub_id, user_type: "master"}).save()
                                        .then(function(){
                                            that.defaultRooms.map(function(v, k){
                                                that.addRoomsToHub({room_name: v, room_description: v}, newHub.hub_id);
                                            });
                                            res.send({status: 1, message: "Hub is added successfully."});
                                        });
                                }else{
                                    res.send({status: 0, message: "This hub details for the user already exist."});
                                }
                            });
                    });
            }else{
                res.send({status: 0, message: "This hub already exist."});
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message});
        });
};


Hubs.prototype.addNewHub = function(req, res){
    var that = this
    , obj = req.body.reqObject.hubDetails
    , emergencyDetails = req.body.reqObject.emergencyDetails
    , definitionDetails = req.body.reqObject.definitionDetails
    , user_id = req.body.user_id;

    if(!obj || !emergencyDetails || !definitionDetails){
        res.send({status: 0, message: "Please provide all the required details to add the hub."});
        return true;
    }

    obj.hub_id = req.body.hub_id;
    obj.password = req.body.password;
    definitionDetails.hub_id = req.body.hub_id;
    _.each(emergencyDetails, function(v, k){
        emergencyDetails[k].hub_id = req.body.hub_id;
    });
    // obj.model_no = req.body.reqObject.hubDetails.model_no;
    // obj.date_of_mfg = req.body.date_of_mfg;
    // obj.software_version = req.body.software_version;
    // obj.software_install_date = req.body.software_install_date;

    if(emergencyDetails.length > Globals.maxContacts){
        res.send({status: 0, message: "You have reached the Max contacts."});
        return true;
    }
    that.getModels();
    that.hubModel.findAll({where: {hub_id: obj.hub_id}})
        .then(function(hub){
            if(hub.length === 0){
                that.hubModel.build(obj).save()
                    .then(function(newHub){
                        that.hubDefModel.build(definitionDetails).save()
                            .then(function(detailsRec){
                                that.hubEmergConts.bulkCreate(emergencyDetails)
                                    .then(function(recs){
                                        that.userHubModel.findAll({where: {user_id: user_id, hub_id: newHub.hub_id}})
                                            .then(function(hubs){
                                                if(hubs.length === 0){
                                                    that.userHubModel.build({user_id: user_id, hub_id: newHub.hub_id, user_type: "master"}).save()
                                                        .then(function(){
                                                            // that.defaultRooms.map(function(v, k){
                                                            //     that.addRoomsToHub({room_name: v, room_description: v}, newHub.hub_id);
                                                            // });
                                                            res.send({status: 1, message: "Hub is added successfully."});
                                                        });
                                                }else{
                                                    res.send({status: 0, message: "This hub details for the user already exist."});
                                                }
                                            });
                                    });
                            });
                    });
            }else{
                res.send({status: 0, message: "This hub already exist."});
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message});
        });
};

Hubs.prototype.addNewHublatest = function(req, res){
    var that = this
    , obj = req.body.reqObject.hubDetails
    , emergencyDetails = req.body.reqObject.emergencyDetails
    , definitionDetails = req.body.reqObject.definitionDetails
    , user_id = req.body.user_id;

    if(!obj || !emergencyDetails || !definitionDetails){
        res.send({status: 0, message: "Please provide all the required details to add the hub."});
        return true;
    }

    obj.hub_id = req.body.hub_id;
    obj.password = req.body.password;
    definitionDetails.hub_id = req.body.hub_id;
    _.each(emergencyDetails, function(v, k){
        emergencyDetails[k].hub_id = req.body.hub_id;
    });
    // obj.model_no = req.body.reqObject.hubDetails.model_no;
    // obj.date_of_mfg = req.body.date_of_mfg;
    // obj.software_version = req.body.software_version;
    // obj.software_install_date = req.body.software_install_date;

    if(emergencyDetails.length > Globals.maxContacts){
        res.send({status: 0, message: "You have reached the Max contacts."});
        return true;
    }
    that.getModels();
    that.hubModel.findAll({where: {hub_id: obj.hub_id}})
        .then(function(hub){
            if(hub.length === 0){
                that.hubModel.build(obj).save()
                    .then(function(newHub){
                        that.hubDefModel.build(definitionDetails).save()
                            .then(function(detailsRec){
                                that.hubEmergConts.bulkCreate(emergencyDetails)
                                    .then(function(recs){
                                        that.userHubModel.findAll({where: {user_id: user_id, hub_id: newHub.hub_id}})
                                            .then(function(hubs){
                                                if(hubs.length === 0){
                                                    that.userHubModel.build({user_id: user_id, hub_id: newHub.hub_id, user_type: "master"}).save()
                                                        .then(function(){
                                                            that.addRoomsToHub(req,res,user_id,{room_name: "MyHome", room_description: "Default Room"}, newHub.hub_id);
                                                            // that.defaultRooms.map(function(v, k){
                                                            //     that.addRoomsToHub({room_name: v, room_description: v}, newHub.hub_id);
                                                            // });
                                                           // res.send({status: 1, message: "Hub is added successfully."});
                                                        });
                                                }else{
                                                    res.send({status: 0, message: "This hub details for the user already exist."});
                                                }
                                            });
                                    });
                            });
                    });
            }else{
                res.send({status: 0, message: "This hub already exist."});
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message});
        });
};





Hubs.prototype.UpdateHubFullDetails = function(req, res){
    var that = this
    , obj = req.body.reqObject.hubDetails
    , emergencyDetails = req.body.reqObject.emergencyDetails
    , definitionDetails = req.body.reqObject.definitionDetails
    , user_id = req.body.user_id;

    if(!obj || !emergencyDetails || !definitionDetails){
        res.send({status: 0, message: "Please provide all the required details to add the hub."});
        return true;
    }

    obj.hub_id = req.body.hub_id;

    if(emergencyDetails.length > Globals.maxContacts){
        res.send({status: 0, message: "You have reached the Max contacts."});
        return true;
    }
    that.getModels();
    that.hubModel.findAll({where: {hub_id: obj.hub_id}})
        .then(function(hub){
            if(hub.length !== 0){
                that.hubModel.update(obj, {where: {hub_id: obj.hub_id}})
                    .then(function(newHub){
                        that.hubDefModel.update(definitionDetails, {where: {hub_id: obj.hub_id}})
                            .then(function(detailsRec){
                                var count = 0;
                                _.each(emergencyDetails, function(v, k){
                                    that.hubEmergConts.update(v, {where: {hub_id: obj.hub_id, hub_contact_id: v.hub_contact_id}})
                                        .then(function(recs){
                                            if(++count === emergencyDetails.length){
                                                res.send({status: 1, message: "Hub is added successfully."});
                                            }
                                        });
                                });
                            });
                    });
            }else{
                res.send({status: 0, message: "This hub doesn't exist."});
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message});
        });
};

Hubs.prototype.getHubs = function(req, res){
    var that = this
    , user_id = req.body.user_id;

    that.getModels();
    that.userHubModel.findAll({where: {user_id: user_id, act_enabled: true}, order: 'hub_id DESC'})
        .then(function(hubs){
            if(hubs.length === 0){
                res.send({status: 0, message: "No Hubs added yet. Please add the hubs."});
            }else{
                var hubIds = _.pluck(hubs, "hub_id");
                that.hubDefModel.findAll({where: {hub_id: {$in: hubIds}}, order: 'hub_id DESC'})
                    .then(function(_hubDefs){
                        hubs = _.map(_hubDefs, function(v, k){
                                    definitionDetails = {};
                                    definitionDetails.device_name = v.device_name;
                                    definitionDetails.device_desc = v.device_desc;
                                    definitionDetails.hub_longitude = v.hub_longitude;
                                    definitionDetails.hub_latitude = v.hub_latitude;
                                    hubs[hubIds.indexOf(v.hub_id)].dataValues.definitionDetails = definitionDetails;
                                    return hubs[hubIds.indexOf(v.hub_id)].dataValues;
                                });
                        that.hubModel.findAll({where: {hub_id: {$in: hubIds}}, order: 'hub_id DESC'})
                            .then(function(_hubs){
                                hubs = _.map(_hubs, function(v, k){
                                            hubDetails = {};
                                            hubDetails.model_no = v.model_no;
                                            hubDetails.date_of_mfg = v.date_of_mfg;
                                            hubDetails.software_version = v.software_version;
                                            hubDetails.software_install_date = v.software_install_date;
                                            hubDetails.hub_install_date = v.hub_install_date;
                                            hubs[hubIds.indexOf(v.hub_id)].hubDetails = hubDetails;
                                            return hubs[hubIds.indexOf(v.hub_id)];
                                        });
                                that.hubEmergConts.findAll({where: {hub_id: {$in: hubIds}}, order: 'hub_id DESC'})
                                    .then(function(_hubContacts){
                                        _.each(_hubContacts, function(v, k){
                                            hubs[hubIds.indexOf(v.hub_id)].contacts = hubs[hubIds.indexOf(v.hub_id)].contacts || [];
                                            hubs[hubIds.indexOf(v.hub_id)].contacts.push(v);
                                            // return hubs[hubIds.indexOf(v.hub_id)];
                                        });
                                        res.send({status: 1, message: "connected Hubs are fetched successfully.", resObject: {hubs: hubs}});
                                    })
                            });
                    });
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message});
        });
};

Hubs.prototype.updateHub = function(req, res) {
    var that = this
    , hub_id = req.body.hub_id;

    that.getModels();
    that.hubModel.findAll({where: {hub_id: hub_id}})
        .then(function(hub){
            if(hub.length !== 0){
                that.hubModel.update(req.body.reqObject, {where: {hub_id: hub_id}})
                    .then(function(){
                        res.send({status: 1, message: "Hub details are updated successfully."});
                    });
            }else{
                res.send({status: 0, message: "Hub doesn't exist."});
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message});
        });
};

Hubs.prototype.getHubUsers = function(req, res){
    var that = this
    , hub_id = req.body.hub_id;

    that.getModels();
    // device_name
    that.userHubModel.findAll({where: {hub_id: hub_id, act_enabled: true}, order: 'user_id DESC'})
        .then(function(hubs){
            if(hubs.length === 0){
                res.send({status: 0, message: "No Hubs added yet. Please add the hubs."});
            }else{
                var userIds = _.pluck(hubs, "user_id");
                that.userModel.findAll({where: {user_id: {$in: userIds}}, order: 'user_id DESC'})
                    .then(function(users){
                        users = _.map(users, function(v, k){
                                    hubs[userIds.indexOf(v.user_id)].dataValues.email_id = v.email_id;
                                    return hubs[userIds.indexOf(v.user_id)].dataValues;
                                });
                        res.send({status: 1, message: "connected Hubs are fetched successfully.", resObject: {hubs: users}});
                    });
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message});
        });
};

Hubs.prototype.addHubEmergencyDetails = function(req, res){
    // emergency_name, emergency_phone, emergency_email
    var that = this
    , obj = {}
    obj.hub_id = req.body.hub_id;
    for(var k in req.body.reqObject){
        obj[k] = req.body.reqObject[k];
    }

    that.getModels();
    that.hubModel.findAll({where: {hub_id: obj.hub_id}})
        .then(function(hub){
            if(hub.length !== 0){
                that.hubEmergConts.findAll({where: {hub_id: obj.hub_id}})
                    .then(function(hubEContacts){
                        if(hubEContacts.length <= Globals.maxContacts){
                            that.hubEmergConts.build(obj).save()
                                .then(function(){
                                    res.send({status: 1, message: "Emergency contact details of the hub are saved successfully."});
                                });
                        }else{
                            res.send({status: 0, message: "You have reached the Max contacts."});
                        }
                    });
            }else{
                res.send({status: 0, message: "Hub doesn't exist."});
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message});
        });
};

Hubs.prototype.addHubDefDetails = function(req, res){
    // device_name, device_desc, hub_longitude, hub_latitude
    var that = this
    , obj = {}
    obj.hub_id = req.body.hub_id;
    for(var k in req.body.reqObject){
        obj[k] = req.body.reqObject[k];
    }

    that.getModels();
    that.hubModel.findAll({where: {hub_id: obj.hub_id}})
        .then(function(hub){
            if(hub.length !== 0){
                that.hubDefModel.findAll({where: {hub_id: obj.hub_id}})
                    .then(function(cont){
                        if(cont.length === 0){
                            that.hubDefModel.build(obj).save()
                                .then(function(){
                                    res.send({status: 1, message: "Hub Definition details saved successfully."});
                                });
                        }else{
                            res.send({status: 0, message: "Hub Definition details already exist."});
                        }
                    });
            }else{
                res.send({status: 0, message: "Hub doesn't exist."});
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message});
        });
};

Hubs.prototype.getHubContactDetails = function(req, res){
    var that = this
    , hub_id = req.body.hub_id;

    that.getModels();
    that.hubModel.findAll({where: {hub_id: hub_id}})
        .then(function(hubs){
            if(hubs.length === 0){
                res.send({status: 0, message: "Hub doesn't exist."});
            }else{
                that.hubEmergConts.findAll({where: {hub_id: hub_id}})
                    .then(function (hubDetails) {
                        if(hubDetails.length === 0){
                            res.send({status: 0, message: "Hub emergency contact details doesn't exist."});
                        }else{
                            res.send({status: 1, message: "Hub emergency contact details are fetched successfully.", resObject: {hubDetails: hubDetails}});
                        }
                    });
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message});
        });
};

Hubs.prototype.getHubDefDetails = function(req, res){
    var that = this
    , hub_id = req.body.hub_id;

    that.getModels();
    that.hubModel.findAll({where: {hub_id: hub_id}})
        .then(function(hubs){
            if(hubs.length === 0){
                res.send({status: 0, message: "Hub doesn't exist."});
            }else{
                that.hubDefModel.findAll({where: {hub_id: hub_id}})
                    .then(function (hubDetails) {
                        if(hubDetails.length === 0){
                            res.send({status: 0, message: "Hub Definition details doesn't exist."});
                        }else{
                            res.send({status: 1, message: "Hub Definition details are fetched successfully.", resObject: {hubDetails: hubDetails}});
                        }
                    });
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message});
        });
};

Hubs.prototype.updateHubContactDetails = function(req, res){
    // emergency_name, emergency_phone, emergency_email
    var that = this
    , obj = req.body.reqObject
    , hub_id = req.body.hub_id
    , hub_contact_id = req.body.hub_contact_id;

    that.getModels();
    that.hubModel.findAll({where: {hub_id: hub_id}})
        .then(function(hub){
            if(hub.length !== 0){
                that.hubEmergConts.findAll({where: {hub_id: hub_id, hub_contact_id: hub_contact_id}})
                    .then(function(cont){
                        if(cont.length === 0){
                            res.send({status: 0, message: "Hub emergency contact details doesn't exist."});
                        }else{
                            that.hubEmergConts.update(obj, {where: {hub_contact_id: hub_contact_id}})
                                .then(function(){
                                    res.send({status: 1, message: "Emergency contact details of the hub are updated successfully."});
                                });
                        }
                    });
            }else{
                res.send({status: 0, message: "Hub doesn't exist."});
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message});
        });
};

Hubs.prototype.connectHub = function(req, res){
    var that = this
    , hub_id = req.body.hub_id
    , user_id = req.body.user_id
    , password = User.encryptPassword(req.body.password);

    that.getModels();
    that.userHubModel.findAll({where: {user_id: user_id, hub_id: hub_id}})
        .then(function(usrRec){
            if(usrRec.length === 0){
                res.send({status: 0, message: "Unauthorized authentication failed."});
            }else{
                that.hubModel.findAll({where: {hub_id: hub_id, password: password}})
                    .then(function(hubs){
                        if(hubs.length === 0){
                            res.send({status: 0, message: "Hub is not added. Please contact admin to add hub."});
                        }else{
                            redisDb.isKeyExists("hub_id_"+hub_id, function(bool){
                                var sessionId = that.generateSessionId();
                                if(bool){
                                    redisDb.setData("hub_id_"+hub_id, {sessionId: sessionId}, function(err, obj){
                                        if(err){
                                            res.send({status: 0, message: "Unable connect the hub."});
                                            // res.send({status: 0, message: "Something went wrong in redis. Please contact admin."});
                                        }else{
                                            res.send({status: 1, message: "Connected hub successfully.", sessionId: sessionId});
                                        }
                                    });
                                }else{
                                    redisDb.delData("hub_id_"+hub_id, function(err, reply){
                                        if(err){
                                            res.send({status: 0, message: "Unable connect the hub."});
                                        }else{
                                            logger.info("Deleting existing key");
                                            redisDb.setData("hub_id_"+hub_id, {sessionId: sessionId}, function(err, obj){
                                                logger.info("Adding key");
                                                if(err){
                                                    res.send({status: 0, message: "Unable connect the hub."});
                                                }else{
                                                    res.send({status: 1, message: "Connected hub successfully.", sessionId: sessionId});
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
            }
        })
        .catch(function(err){
            logger.info("falied to connect hub.");
            res.send({status: 1, message: "falied to connect hub."});
        });
};

Hubs.prototype.addRoomsToHub = function(req,res,userId,obj, hub_id){
    var that = this;
     var room_id_out;
     var dObj={};
    that.roomModel.build(obj).save()
        .then(function(room){
            that.hubRoomModel.build({hub_id: hub_id, room_id: room.room_id}).save()
                .then(function(){
                              room_id_out=room.room_id;
                                            dObj.category_id = "70a2bbe3-505c-4828-8311-2c2fc3bbc755";
                                            dObj.protocol = "";
                                            dObj.room_id=room.room_id;
                                           that.deviceAdd(req,res,dObj,hub_id);

                });
        })
        .catch(function(err){
            logger.info("falied to add the room");
        });
};

Hubs.prototype.getAllUserDetails = function(req, res) {
    var that = this
        , user_id = req.body.user_id
        , resObject = {}
        , count = 0;

    that.getModels();
    that.userHubModel.findAll({where: {user_id: user_id}})
        .then(function(data){
            if(data.length === 0){
                res.send({status: 1, message: "data retrieved successfully."});
            }else{
                resObject.userHubs = data;
                var hubs = _.pluck(data, "hub_id");
                for(var i in hubs){
                    that.hubModel.findAll({where: {hub_id: hubs[i]}})
                        .then(function(data1){
                            resObject.hubs = resObject.hubs || [];
                            resObject.hubs.push(data1[0]);
                        });
                    that.hubDefModel.findAll({where: {hub_id: hubs[i]}})
                        .then(function(data2){
                            resObject.hubDefs = resObject.hubDefs || [];
                            resObject.hubDefs.push(data2);
                        });
                    that.hubEmergConts.findAll({where: {hub_id: hubs[i]}})
                        .then(function(data3){
                            resObject.hubContacts = resObject.hubContacts || [];
                            resObject.hubContacts.push(data3);
                        });
                    that.userHubModel.findAll({attributes: ["device", "music", "user_id", "remote", "security", "user_type"], where: {hub_id: hubs[i], parent_id: user_id}})
                        .then(function(userhub){
                            resObject.guests = resObject.guests || [];
                            if (userhub.length === 0) {
                                resObject.guests.push(userhub);
                            } else {
                                var arrUser = _.pluck(userhub, "user_id");
                                that.userModel.findAll({attributes: ["user_id", "email_id"], where: {user_id: {$in : arrUser}}, order: 'user_id DESC'})
                                    .then(function(usrArr){
                                        for(var i = 0; i < userhub.length; i++){
                                            userhub[i].dataValues.email_id = usrArr[i].email_id;
                                        }
                                        userhub = _.chain(userhub).pluck("dataValues").sortBy('email_id').value();
                                        resObject.guests.push(userhub);
                                    });
                            }
                        });
                    that.hubRoomModel.findAll({where: {hub_id: hubs[i]}})
                        .then(function(rooms){
                            if(rooms.length === 0){
                                resObject.rooms = resObject.rooms || [];
                                resObject.rooms.push([]);
                                resObject.devices = resObject.devices || [];
                                resObject.devices.push([]);
                                count = that.updateCount(count, res, data, resObject);
                            }else{
                                rooms = _.pluck(rooms, "room_id");
                                that.roomModel.findAll({where: {room_id: {$in: rooms}}})
                                    .then(function(roomsObj){
                                        resObject.rooms = resObject.rooms || [];
                                        resObject.rooms.push(roomsObj);
                                    });
                                that.deviceModel.findAll({where: {room_id: {$in: rooms}, device_state: "ADDED"}})
                                    .then(function(devices){
                                        resObject.devices = resObject.devices || [];
                                        if(devices.length === 0){
                                            resObject.devices.push([]);
                                            count = that.updateCount(count, res, data, resObject);
                                        }else{
                                            var categoryArr = _.pluck(devices, "category_id");
                                            that.deviceCategoryModel.findAll({attributes: ['category_id', 'category_type', 'category_name', 'device_radio_type'], where: { category_id: {$in : categoryArr }}})
                                                .then(function(category){
                                                    var reqCategories = _.pluck(category, 'category_id');
                                                    var reqDevices = _.chain(devices)
                                                                        .map(function(dev){ if(reqCategories.indexOf(dev.category_id) !== -1){
                                                                            dev.dataValues.category_type = category[reqCategories.indexOf(dev.category_id)].category_type ;
                                                                            dev.dataValues.category_name = category[reqCategories.indexOf(dev.category_id)].category_name ;
                                                                            dev.dataValues.device_radio_type = category[reqCategories.indexOf(dev.category_id)].device_radio_type ;
                                                                            return dev.dataValues;
                                                                        }})
                                                                        .compact()
                                                                        .sortBy('device_b_one_id')
                                                                        .value();

                                                    var roomIds = _.pluck(reqDevices, 'room_id');
                                                    that.roomModel.findAll({attributes: ['room_id', 'room_name'], where: {room_id: {$in: roomIds }}})
                                                        .then(function(rooms){
                                                            _.chain(rooms)
                                                                .map(function(roomRec){
                                                                    var arr = roomIds.reduce(function(a, e, i) {
                                                                                    if (e === roomRec.dataValues.room_id)
                                                                                        a.push(i);
                                                                                    return a;
                                                                                }, []);
                                                                    _.map(arr, function(v, k){ reqDevices[v]['room_name'] = roomRec.dataValues.room_name; });
                                                                })
                                                            resObject.devices.push(reqDevices);
                                                            count = that.updateCount(count, res, data, resObject);
                                                        });
                                                });
                                        }
                                    });
                            }
                        });
                }
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message});
        });
};

Hubs.prototype.updateCount = function(count, res, data, resObject) {
    count++
    if(count === data.length){
        res.send({status: 1, message: "data retrieved successfully.", userData: resObject});
    }
    return count;
};

Hubs.prototype.getDevicesCount = function(req, res) {
    var that = this;
    that.getModels();
    that.hubRoomModel.findAll({where: {hub_id: req.body.hub_id}})
        .then(function(data){
            if(data.length === 0){
                res.send({status: 1, message: "Retrieved devices count successfully.", count: 0});
            }else{
                var rooms = _.pluck(data, 'room_id');
                that.deviceModel.findAll({where: {room_id: {$in: rooms}}})
                    .then(function(_data){
                        res.send({status: 1, message: "Retrieved devices count successfully.", count: _data.length});
                    });
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message});
        });
};

Hubs.prototype.generateSessionId = function() {
    var sha = crypto.createHash('sha1');
    sha.update(Math.random().toString());
    return sha.digest('hex');
};

Hubs.prototype.getModels = function(){
    if(this.hubModel){
        return ;
    }
    var Models = db.getModels();
    this.hubModel = Models.Hub;
    this.userHubModel = Models.UserHub;
    this.userModel = Models.User;
    this.hubDefModel = Models.HubDef;
    this.roomModel = Models.Room;
    this.hubRoomModel = Models.HubRoom;
    this.hubEmergConts = Models.HubEmergConts;
    this.deviceModel = Models.Devices;
    this.deviceCategoryModel = Models.DeviceCategory;
};

//Doc:  creating the  dummy hubid
Hubs.prototype.generateHubId = function(req, res){
  if(this.dummyHubCounter.toString().length > globals.maxDummyHubsDigitLength){
    res.send({status: 0, message: "Crossed Maximum allowed dummy hubs."});
  }else {
    var dummyHubCounter = "$$" + ("0000000000" + this.dummyHubCounter).slice(-1*globals.maxDummyHubsDigitLength);
    this.dummyHubCounter += 1;
    res.send({status: 1, hub_id: dummyHubCounter});
  }
};

Hubs.prototype.getDummyHubslength = function(){
  var that = this;
  that.getModels();
  that.hubModel.findall({where: {hub_id: {$like: "$$%"}}, order: 'hub_install_date DESC'})
      .then(function(hubs){
          if(hubs.length){
            that.dummyHubCounter = parseInt(hubs[0].replace("$$"))+1;
          }else{
            that.dummyHubCounter = 0;
          }
      }).catch(function(err){
          logger.error("Error while fetching the dummyhubid's");
      });
};


Hubs.prototype.getHubState = function(req, res){

   var that = this;
  that.getModels();

  that.hubModel.findAll({where: {hub_id: req.body.hub_id }})
    .then(function(user){
      if(user.length == 0){
        res.send({status: 0, message: "hub Does not exist."});
      }else {
      // logger.info("my hub data state "+ JSON.stringify(user[0]));
        var _user = _.map(user, function(v){
          var _v = {};
          _v.hub_id = v.hub_id;
          _v.hub_status = v.hub_status;
          _v.hub_status_date = Utils.getTimeStampFromString(v.hub_status_date);
          return _v;
        });
        res.send({status: 1, data: _user});

      }
    })
    .catch(function(err){
        res.send({status: 0, message: err.message });
        logger.error(""+err.message)
    });


};

Hubs.prototype.getallHubstate = function(req, res){

   var that = this;
  that.getModels();

  that.hubModel.findAll({})
    .then(function(user){

//      logger.info("my hub data state "+ JSON.stringify(user));

        res.send({status: 1, data:user});


    })
    .catch(function(err){
        res.send({status: 0, message: err.message });
        logger.error(""+err.message)
    });


};


Hubs.prototype.deviceAdd=function(req,res,obj, hub_id)
{
   var security_id_out,noti_id_out,acty_id_out;
   var that = this;
   that.getModels();
   obj.device_name = "Hub";
   obj.device_b_one_id = "0000";

   that.deviceModel.findAll({where: {room_id: obj.room_id, device_b_one_id: obj.device_b_one_id}})
                                .then(function(devs){
                                    if(devs.length === 0){
                                        that.deviceModel.build(obj).save()
                                            .then(function(devRec){
                                                security_id_out=devRec.device_id;
                                                obj.device_name = "NOTIFICATIONS";
                                                obj.device_b_one_id = "NOTI";
                                                 that.deviceModel.build(obj).save()
                                                 .then(function(notiRec){
                                                    noti_id_out=notiRec.device_id;
                                                    obj.device_name = "ACTIVITY FEED";
                                                    obj.device_b_one_id = "ACTY";
                                                     that.deviceModel.build(obj).save()
                                                     .then(function(actyRec){
                                                       acty_id_out=actyRec.device_id;

                                                       that.getAllUserDetails(req,res);
                                                                   logger.info("room id is "+obj.room_id);
                                                                    logger.info("0000 id is "+security_id_out);
                                                                    logger.info("noti id is "+noti_id_out);
                                                                    logger.info("acty id is "+acty_id_out);
                                                       //
                                                      //              optobj={};
                                                      //              optobj.MyHome=obj.room_id;
                                                      //              optobj.Securuty=security_id_out;
                                                      //              optobj.NOTI=noti_id_out;
                                                      //              optobj.ACTY=acty_id_out;
                                                       //
                                                      //  res.send({status: 1, message: "Default Devices Added Sucessfully", devicesinfo : optobj });



                                                     });


                                                 });

                                              //  res.send({status: 1, message: "Device added successfully.", device_id: devRec.device_id });
                                            });
                                    }else{
                                        res.send({status: 0, message: "The device with the given B.One id already exist."});
                                    }
                                })

                    .catch(function(err){
                        res.send({status: 0, message: err.message });
                    });

};




module.exports = new Hubs();
