var amqp = require('amqp'),
        Globals = require('../../globals')
        , log4js = require('log4js')
        , _ = require('underscore');

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

QueueHandler = function () {
    if (QueueHandler._instance)
        return QueueHandler._instance;
    // this.send = function(queueName, message){
    //     var connectionObj = this,
    //         exchangeObj = false,
    //         publishObj = false,
    //         exchangeOptions = {durable: true, autoDelete: false, type: 'fanout', confirm: true},
    //         publishOptions = {"Content-Type": "application/json"};

    //     connectionObj.on('ready', function () {
    //         mailExchangeObj = connectionObj.exchange(queueName+"Exchange", exchangeOptions);
    //         mailExchangeObj.on("open", function () {
    //             mailPublishObj = mailExchangeObj.publish(queueName, jsonObject, publishOptions);
    //             mailPublishObj.on("ack", function () {
    //                 logger.info("Your message is publish on queue i.e)" + queueName);
    //             });
    //             mailPublishObj.on("error", function () {
    //                 logger.info("Your message not published on queue i.e)" + queueName + " due to some error");
    //             });
    //         });
    //     });
    // }
    _.bindAll(this, "sendMail", "pushNotification", 'eventQueue');
    this.__proto__ = amqp.createConnection({host: Globals.RabbitMQConfig.host, port: Globals.RabbitMQConfig.port, login: Globals.RabbitMQConfig.auth.user, password: Globals.RabbitMQConfig.auth.pass});
    QueueHandler._instance = this;

    QueueHandler._instance.constructor = null;
};

QueueHandler.getInstance = function(port, ip) {
    if (QueueHandler._instance) {
        console.log("inside if");
        return QueueHandler._instance;
    }
    console.log("next to if");
    return new QueueHandler();
};

QueueHandler.prototype.sendMail = function (jsonObject) {
    var connectionObj = this,
        mailExchangeObj = false,
        mailPublishObj = false,
        mailExchangeOptions = {durable: true, autoDelete: false, type: 'fanout', confirm: true},
        mailPublishOptions = {"Content-Type": "application/json"};
    // console.log(connectionObj);
    // connectionObj.on('ready', function () {
        mailExchangeObj = connectionObj.exchange(Globals.RabbitMQConfig.ExchangeName, mailExchangeOptions);
        mailExchangeObj.on("open", function () {
            mailPublishObj = mailExchangeObj.publish(Globals.RabbitMQConfig.QueueName, jsonObject, mailPublishOptions);
            mailPublishObj.on("ack", function () {
                logger.info("Your message is publish on queue i.e)" + Globals.RabbitMQConfig.QueueName);
                mailExchangeObj.close();
               // connection.destroy();
            });
            mailPublishObj.on("error", function () {
                logger.info("Your message not published on queue i.e)" + Globals.RabbitMQConfig.QueueName + " due to some error");
                mailExchangeObj.close();
                //connection.destroy();
            });
        });
    // });
};

QueueHandler.prototype.pushNotification = function (jsonObject) {
    var connectionObj = this,
        notificationExchangeObj = false,
        notificationPublishObj = false,
        notificationExchangeOptions = {durable: true, autoDelete: false, type: 'fanout', confirm: true},
        notificationPublishOptions = {"Content-Type": "application/json"};

        //  console.log("sujit");   logger.info("sujit comming here.");
    // connectionObj.on('ready', function () {
             notificationExchangeObj = connectionObj.exchange(Globals.RabbitMQConfig.PushNotificationExchangeName, notificationExchangeOptions);
        notificationExchangeObj.on("open", function () {
            notificationPublishObj = notificationExchangeObj.publish(Globals.RabbitMQConfig.PushNotificationQueueName, jsonObject, notificationPublishOptions);
            notificationPublishObj.on("ack", function () {
                // console.log("sujit push msg");    logger.info("sujit push ok"+JSON.stringify(jsonObject));
                console.log("Your message is publish on queue i.e)" + Globals.RabbitMQConfig.PushNotificationQueueName);
                notificationExchangeObj.close();
                //connection.destroy();
            });
            notificationPublishObj.on("error", function () {
                      //  logger.info("sujit error msg");
                console.log("Your message not published on queue i.e)" + Globals.RabbitMQConfig.PushNotificationQueueName + " due to some error");
                notificationExchangeObj.close();
                //connection.destroy();
            });


        });

    // });
};

QueueHandler.prototype.eventQueue = function (jsonObject) {
    var connectionObj = this,
        notificationExchangeObj = false,
        notificationPublishObj = false,
        notificationExchangeOptions = {durable: true, autoDelete: false, type: 'fanout', confirm: true},
        notificationPublishOptions = {"Content-Type": "application/json"};

    // connectionObj.on('ready', function () {
        notificationExchangeObj = connectionObj.exchange(Globals.RabbitMQConfig.EventExchangeName, notificationExchangeOptions);
        notificationExchangeObj.on("open", function () {
            notificationPublishObj = notificationExchangeObj.publish(Globals.RabbitMQConfig.EventQueueName, jsonObject, notificationPublishOptions);
            notificationPublishObj.on("ack", function () {
                console.log("Your message is publish on queue i.e)" + Globals.RabbitMQConfig.EventQueueName);
                notificationExchangeObj.close();
            });
            notificationPublishObj.on("error", function () {
                console.log("Your message not published on queue i.e)" + Globals.RabbitMQConfig.EventQueueName + " due to some error");
                notificationExchangeObj.close();
            });
        });
    // });
};
module.exports = QueueHandler.getInstance();
