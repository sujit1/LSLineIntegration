var express = require('express')
    , router = express.Router()
    , _ = require('underscore')
    , Globals = require('../../globals')
    , countryData = require('country-data');

Date.prototype.getUTCTime = function(){
  return new Date(
    this.getUTCFullYear(),
    this.getUTCMonth(),
    this.getUTCDate(),
    this.getUTCHours(),
    this.getUTCMinutes(),
    this.getUTCSeconds()
  ).getTime();
};

function Utils() {
    router.get('/getCountries', _.bind(this.getCountries, this));
    this.router = router;
    return this;
};
Utils.prototype.getCountries = function (req, res) {
    var obj = {}, arr = [];
    countryData.callingCountries.all.map(function (val) {
        if (val.countryCallingCodes.length !== 0) {
            obj[val.name.replace(/ /g, "")] = {name: val.name, countryCallingCodes: val.countryCallingCodes[0].replace(/ /g, "")};
        }
    });
    arr = Object.keys(obj).sort().map(function (v) {
        return obj[v];
    });
    res.send({status: 1, countries: arr});
};

Utils.prototype.isOriginHub = function(origin_id){
    // origin_id = 0 for hub and 1 for app and 2 for customer care support
    if(origin_id == 0){
        return true;
    }
    return false;
}

Utils.prototype.isOriginApp = function(origin_id){
    // origin_id = 0 for hub and 1 for app and 2 for customer care support
    if(origin_id == 1){
        return true;
    }
    return false;
}

Utils.prototype.getTimeStamp = function(date) {
  // date = "2016-03-05T16:25:10.250Z";
  return [ ("00"+(date.getMonth()+1)).slice(-2), ("00"+date.getDate()).slice(-2), ("00"+date.getFullYear()).slice(-4) ].join('/') + ' ' +
  [ ("00"+date.getHours()).slice(-2), ("00"+date.getMinutes()).slice(-2), ("00"+date.getSeconds()).slice(-2) ].join(':');
};

Utils.prototype.getTimeStampFromString = function (str) {
  var date = str.split(" ");
  var time = date[1] ? date[1].split(":") : 0;
  date = new Date(date[0]);
  time = (parseInt(time[0])*3600 + parseInt(time[1])*60 + parseInt(time[2])) * 1000;
  time = this.getTimeStamp(new Date(date.getUTCTime() + time));
  return time;
};

module.exports = new Utils();
