var express = require('express')
    , router = express.Router()
    , _ = require('underscore')
    , Globals = require('../../globals')
    , QueueHandler = require('../Commons/QueueHandler')
    , mysqlDb = require('../../mySqlDB')
    , db = require('../../db')
    , User = require('../User/Users')
    , Globals = require('../../globals')
    , log4js = require('log4js')
    , request = require("request")
    , Event= require('../Events/Events')
    , lsDevice=require('../Device/Device')
    , line = require('@line/bot-sdk');


  const config = {
  channelAccessToken:'GDOLGAXMKk2v/KbIuLk7fqcK8HFe6EneL8Lw6WyvqK58TC3EyZg17vmxQnY8juggQ0NiCW6HtW2yWVpHBHQGFJYGdHRhggtR9J7koDTyQT2i7jPwiM9khRbdkNyJH/I3swFhUUgMAnzmj1d8X36bFgdB04t89/1O/w1cDnyilFU=',//process.env.CHANNEL_ACCESS_TOKEN,
  channelSecret:'814625b04c9c66c6a96f13bd3685f9c7', // process.env.CHANNEL_SECRET,
};

// create LINE SDK client
const client = new line.Client(config);


const createSignature = (secret, body) => {
  return crypto
    .createHmac('sha256', secret)
    .update(body)
    .digest('base64');
};


log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

function Webhook() {
    _.bindAll(this, "webhookfunction","checkSignature","getModels","securityCall","postBackcall","sensorCall");

    router.post('/webhook',this.checkSignature);


    return router;
};

Webhook.prototype.checkSignature = function(req, res) {
  line.middleware(config);
  this.webhookfunction(req,res);
};

Webhook.prototype.webhookfunction = function(req, res) {
    var that = this;


Promise
.all(req.body.events.map(function(event){
  that.handleEvent(event);

}))


};



Webhook.prototype.handleEvent=function (event) {
    var that=this;
  //  variable declaration
  var hub_id,user_id,echo;
    //printing the event request
    logger.info("event comming here   ::: "+JSON.stringify(event));
    //get the line user id
    var lineId=event.source.userId;
    var atpos,dotpos,x;

   // checkigng the Line user

     that.getModels();
     // email condition check
       if(event.type === "message")
       {
         x=event.message.text.toString();
         atpos = x.indexOf("@");
         dotpos = x.lastIndexOf(".");
       }
       else {
         atpos=-1;
         dotpos=-1;
       }



      if(atpos === -1 && dotpos === -1)
      {
        if(event.type === "message" && (event.message.text==="Hi" || event.message.text==="Hello"))
        {
          echo={ type: 'text', text: "Hi. Thank you for adding me to your friendlist. You can either use the dashbaord, or ask me about your LS. I am trying to become as smart as you!" };
          logger.error("data sending :" + JSON.stringify(echo));
          return client.replyMessage(event.replyToken, echo);

        }
        else {


        that.lineModel.findAll({where: {line_id: lineId}})
                        .then(function(user){

                          if(user.length>0)
                          {
                            hub_id=user[0].hub_id;
                            user_id=user[0].user_id;
                            //echo={ type: 'text', text: "you have conformed the email id " };
                           //  logger.error("data sending :" + JSON.stringify(echo));
                            //return client.replyMessage(event.replyToken, echo);

                              if(event.type === "message" && (event.message.text==="Security" || event.message.text==="I want to check the security details of my house"))
                              {
                                 that.securityCall(user_id,hub_id,event.replyToken);
                              }
                               if(event.type === 'postback')
                              {
                                 that.postBackcall(user_id,hub_id,event.postback.data,event.replyToken)
                              }

                              if(event.type === "message" && event.message.text==="Sensors" )
                              {
                                that.sensorCall(user_id,hub_id,event.replyToken);
                              }




                          }
                          else {
                             echo={ type: 'text', text: "Please verify your email address." };
                             logger.error("data sending :" + JSON.stringify(echo));
                             return client.replyMessage(event.replyToken, echo);
                          }

                        });

           }
      //end of the email check
      }
      else {

        that.userModel.findAll({where: {email_id:x} })
          .then(function (arr) {

              if(arr.length>0)
              {
                   user_id=arr[0].user_id;
                   logger.info("user id is :"+user_id);
                   that.userHubModel.findAll({where: {user_id:user_id} })
                     .then(function (userhub) {
                      hub_id=userhub[0].hub_id;

                      that.lineModel.build({user_id: user_id, hub_id: hub_id,line_id : lineId,email_id:x}).save()
                          .then(function(){
                            echo={ type: 'text', text: "Thank you for verifying your email address. Let me generate your report." };
                            logger.error("data sending :" + JSON.stringify(echo));
                            return client.replyMessage(event.replyToken, echo);
                          });

                     });


              }
              else {

                echo = {
                          "type": "template",
                          "altText": "this is a buttons template",
                          "template": {
                             "type": "buttons",
                             "thumbnailImageUrl": "https://line.b1automation.com/images/linelogo.png",
                             "title": " you are a new user",
                             "text": "Please find the link to download the app",
                             "actions": [
                                           {
                                             "type": "uri",
                                             "label": "Android",
                                             "uri": "https://play.google.com/store/apps/details?id=com.blaze.admin.blazeandroid&hl=en"
                                           },
                                           {
                                             "type": "uri",
                                             "label": "ios",
                                             "uri": "http://example.com/page/123"
                                           }
                                        ]
                                      }
                          };
                logger.error("data sending :" + JSON.stringify(echo));
                return client.replyMessage(event.replyToken, echo);

              }

          });

      //end of else condition of email check
      }


};


//security call

Webhook.prototype.securityCall = function(user_id,hub_id,rToken){
var that =this;
var timestamp;
var tmbImg;
var msg;
var label1,label2;
var act1,act2;
 that.getModels();
 var query={"hub_id":hub_id,"device_b_one_id":"0000"};
 db.findSortedObjects('states',query,{timestamp:-1},function(err,data){
     logger.error("sujit test"+ JSON.stringify(data));
             //  msg=""+data[0].security_mode;
          timestamp=" "+data[0].security_timestamp;
          logger.info(""+timestamp);
                switch(data[0].security_mode)
                 {
                         case "0":
                         case "DISARM":   msg="DISARM";
                                   label1="Click To Arm";
                                   label2="Click To Inhouse";
                                   act1="arm";
                                   act2="inhouse";
                                   tmbImg="https://line.b1automation.com/images/disarmed.png";
                                   break;
                         case  "1":
                         case  "ARM":           msg="ARM";
                                   label1="Click To Disarm";
                                   label2="Click To Inhouse";
                                   act1="disarm";
                                   act2="inhouse";
                                   tmbImg="https://line.b1automation.com/images/armed.png";
                                   break;

                         case "2":
                         case "INHOUSE":          msg="InHouse";
                                   label1="Click To Arm";
                                   label2="Click To Disarm";
                                   act1="arm";
                                   act2="disarm";
                                   tmbImg="https://line.b1automation.com/images/inhouse.png";

                                   break;
                 }

                         echo= {
                                    "type": "template",
                                    "altText": "this is a buttons template",
                                    "template": {
                                     "type": "buttons",
                                     "thumbnailImageUrl": tmbImg,
                                     "title": msg,
                                     "text":timestamp,
                                     "actions": [
                                         {
                                           "type": "postback",
                                           "label": label1,
                                           "data": act1
                                         },
                                         {
                                           "type": "postback",
                                           "label": label2,
                                           "data": act2
                                         }
                                     ]
                                    }
                                    };


            logger.info("echo msg ::"+ JSON.stringify(echo));

            return client.replyMessage(rToken, echo);
},1);


};


//sensor Call
Webhook.prototype.sensorCall = function(user_id,hub_id,rToken){

var query;
  var options = {
                        url: "https://line.b1automation.com/device/getDevices",
                        method: 'POST',
                        port:443,
                        headers: {
                          'Content-Type': 'application/json',
                          'Accept':'application/json',

                        },
                        json:{"hub_id":hub_id,"category_type":"Security"}
                 };

                 request(options, function(err, res, resbody){

          if(!err)
          {

           //logger.info("data from server :"+JSON.stringify(resbody));

                if(resbody.status=== 0)
                {
                  echo = { type: 'text', text: resbody.message };
                   logger.info("data from server :"+JSON.stringify(echo));
                  return client.replyMessage(rToken, echo);
                }
                else {

                     var deviceData=resbody.devices;
                     var requiredData=[];
                    logger.error("count of devices"+ deviceData.length );
                     _.each(deviceData,function(v){

                          if(v.category_id === "3f695ece-9b66-40ee-a80f-c9785468428f" || v.category_id === "627d64f0-2464-491d-8162-48093f1ea47d" )
                          {
                             requiredData.push(v);
                          }


                     });
                     //logger.error("final device count"+ requiredData.length );
query={"hub_id":hub_id,"device_b_one_id":requiredData[0].device_b_one_id};
                     db.findSortedObjects('states',query,{timestamp:-1},function(err,data){
                         logger.error("sujit test"+ JSON.stringify(data));
                                 //  msg=""+data[0].security_mode;
                              timestamp=" "+data[0].timestamp;
                              logger.info(""+timestamp);


                                             echo= {
                                                        "type": "template",
                                                        "altText": "this is a buttons template",
                                                        "template": {
                                                         "type": "buttons",
                                                         "thumbnailImageUrl": "https://line.b1automation.com/images/linelogo.png",
                                                         "title": "open on",
                                                         "text":timestamp,
                                                         "actions": [
                                                             {
                                                               "type": "postback",
                                                               "label": "   ",
                                                               "data": "    "
                                                             }

                                                         ]
                                                        }
                                                        };


                                logger.info("echo msg ::"+ JSON.stringify(echo));

                                return client.replyMessage(rToken, echo);
                    },1);







                }


          }

         });


};


//postBack call

Webhook.prototype.postBackcall = function(user_id,hub_id,actionid,rToken){
var that=this;
var device_id;
var evtObject;
var msg;
  var query={"hub_id":hub_id,"device_b_one_id":"0000"};
  db.findSortedObjects('states',query,{timestamp:-1},function(err,data){
      logger.error("sujit test"+ JSON.stringify(data));
              //  msg=""+data[0].security_mode;
           device_id=" "+data[0].device_id;
          logger.info(""+device_id);



switch(actionid)
                  {
                          case "arm":
                          case "ARM":   msg="set to arm";
                     evtObject=   {
                                    "security_mode":"1"
                                   };
                                    break;
                          case  "disarm":
                          case  "DISARM": msg="set to disarm";
                                          evtObject=   {
                                    "security_mode":"0"
                                   };

                                    break;

                          case "inhouse":
                          case "INHOUSE":          msg="set to InHouse";
                                   evtObject=   {
                                    "security_mode":"2"
                                   };

                                    break;
                  }


          var jsondata={

 "hub_id":hub_id,
 "device_b_one_id":"0000",
 "device_id":device_id,
 "reqObject":evtObject
}
               logger.info("json data is"+JSON.stringify(jsondata));

 var options = {
                       url: "https://line.b1automation.com/event/sendEvent",
                       method: 'POST',
                       port:443,
                       headers: {
                         'Content-Type': 'application/json',
                         'Accept':'application/json',

                       },
                       json:jsondata
                };

        request(options, function(err, res, resbody){

 if(!err)
 {
    if(res.statusCode == 401)
    {
         logger.info("access token changed");
    }
    else {


logger.info("data upadted sucessfully"+JSON.stringify(resbody));
    echo = { type: 'text', text: "Home  is "+msg };
     return client.replyMessage(rToken, echo);

  }
 }

});

 },1);





};


Webhook.prototype.getModels = function(){
    if(this.userModel){
        return ;
    }
    var Models = mysqlDb.getModels();
    this.userModel = Models.User;
    this.userHubModel = Models.UserHub;
    this.userDevModel = Models.UserDev;
    this.hubModel = Models.Hub;
    this.hubDefModel = Models.HubDef;
    this.roomModel = Models.Room;
    this.hubRoomModel = Models.HubRoom;
    this.hubEmergConts = Models.HubEmergConts;
    this.deviceModel = Models.Devices;
    this.remoteModel = Models.RemoteKeyInfo;
    this.alexaUserModel =Models.AlexaUser;
    this.lineModel=Models.LineUser;
};

module.exports = new Webhook();
