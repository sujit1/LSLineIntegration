var express = require('express')
    , router = express.Router()
    , crypto = require('crypto')
    , _ = require('underscore')
    , db = require('../../mySqlDB')
    , speakeasy = require('../../Libs/speakeasy')
    , Globals = require('../../globals')
    , QueueHandler = require('../Commons/QueueHandler')
    , redisDb = require('../../redisDB')
    , mongodb = require('../../db')
    , Utils = require('../Commons/Utils')
    , log4js = require('log4js');

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

function User() {
    _.bindAll(this, "getRecentUsers", "authenticateUser", "loginUser", "registerUser",
                    "logout", "inviteGuest", "deleteChild", "changePassword", "forgotPassword",
                    "getPermissions", "updatePermissions", "getGuestUsers", "validateSession", "saveLogInOut",
                    'deleteUserDetails', 'sendOTP', 'deleteMasterUserHub', 'sendOTPToMasterUser', 'updateUserAddress','loginWebUser','getallcustomers');

    router.get('/recent', this.getRecentUsers);
    router.post('/authenticate', this.authenticateUser);
    router.post('/login', this.loginUser);
    router.post('/register', this.registerUser);
    router.post('/logout', this.logout);
    router.post('/inviteGuest', this.validateSession, this.inviteGuest);
    router.post('/deleteChild', this.validateSession, this.deleteChild);
    router.post('/changePassword', this.validateSession, this.changePassword);
    router.post('/forgotPassword', this.forgotPassword);
    router.post('/getPermissions', this.validateSession, this.getPermissions);
    router.post('/updatePermissions', this.validateSession, this.updatePermissions);
    router.post('/getGuestUsers', this.validateSession, this.getGuestUsers);
    router.post('/deleteUserDetails', this.deleteUserDetails);
    router.post('/deleteMasterUserHub', this.validateSession, this.deleteMasterUserHub);
    router.get('/deleteUser', this.sendOTP);
    router.post('/sendOTPToMasterUser', this.validateSession, this.sendOTPToMasterUser);
    router.post('/updateUserAddress', this.validateSession, this.updateUserAddress);
    router.post('/loginWebUser',this.loginWebUser);    
    router.post('/getallcustomers',this.getallcustomers);

    this.router = router;

    return this;
};
User.prototype.authenticateUser = function (req, res) {
    logger.trace('authenticateUser Start' + JSON.stringify(req.body));
    var that = this;
    var email_id = req.body.email_id.toLowerCase();
    var authentication_code = req.body.authentication_code;
    var app_device_token_id = req.body.app_device_token_id;

    that.getModels();
    that.userModel.findAll({where: {email_id: email_id}})
        .then(function (docs) {
            docs = docs[0];
            if (!docs) {
                res.send({status: 0, message: "User doesn't exist."});
                return;
            }
            if (docs.authentication_code === authentication_code) {
                var sessionId = that.generateSessionId();
                that.SetUserDev(app_device_token_id, docs, sessionId, res, "Authentication Success.", true, req.body.device_type);
            } else {
                res.send({status: 0, message: "Invalid authentication code. Please Try Again."});
            }
        })
        .catch(function(err){
            logger.error(err.message);
            res.send({status: 0, message: "Data Base error: " + err.message + ". Please try again."});
        });
    logger.trace('authenticateUser End');
};

User.prototype.loginUser = function (req, res) {
    logger.trace('loginUser Start' + JSON.stringify(req.body));
    var obj = {email_id: req.body.email_id.toLowerCase()}, that = this, app_device_token_id = req.body.app_device_token_id;
    that.getModels();

    that.userModel.findAll({where: obj})
        .then(function (arr) {
            if (arr.length === 0) {
                res.send({status: 0, message: "User does not exist."});
            } else {
                arr = arr[0];
                // if (!arr.act_enabled) {
                //     res.send({status: 0, message: "Your account has been de-activated."});
                //     return;
                // }

                if (!arr.act_authenticated) {
                    if (arr.isMaster) {
                        that.sendMail(req.headers.host, arr, Globals.MailerAuthenticationSubject, {email: arr.email_id, templateName: Globals.template.registration});
                        res.send({status: 1, message: ("Please enter the authentication code sent to your registered email."), user: arr, sessionId: null});
                    } else {
                        if (arr.password !== that.encryptPassword(req.body.password)){
                          if(((arr.otp && arr.otp !== that.encryptPassword(req.body.password)) || !arr.otp)) {
                            res.send({status: 0, message: "Password Incorrect."});
                            return ;
                          }else{
                            var _arr = that.checkPassword(arr, arr.otp);
                            arr.update(_arr[1]);
                          }
                        }
                        var sessionId = that.generateSessionId();
                        that.SetUserDev(app_device_token_id, arr, sessionId, res, "Login Success.", false, req.body.device_type);
                    }
                } else if (arr.password !== that.encryptPassword(req.body.password)){
                    if(((arr.otp && arr.otp !== that.encryptPassword(req.body.password)) || !arr.otp)) {
                      res.send({status: 0, message: "Password Incorrect."});
                      return ;
                    }else{
                      var _arr = that.checkPassword(arr, arr.otp);
                      arr.update(_arr[1]);
                      var sessionId = that.generateSessionId();
                      that.SetUserDev(app_device_token_id, arr, sessionId, res, "Login Success.", true, req.body.device_type);
                    }
                } else {
                    var sessionId = that.generateSessionId();
                    that.SetUserDev(app_device_token_id, arr, sessionId, res, "Login Success.", true, req.body.device_type);
                }
            }
        })
        .catch(function(err){
            logger.error(err.message);
            res.send({status: 0, message: "Data Base error: " + err.message + ". Please try again."});
        });
    logger.trace('loginUser End');
};







User.prototype.getRecentUsers = function (req, res) {
    logger.trace('getRecentUsers Start' + JSON.stringify(req.body));
    this.getModels();
    this.userModel.findAll().then(function (docs) {
        res.send({users: docs});
    });
    logger.trace('getRecentUsers End');
};

User.prototype.registerUser = function (req, res) {
    logger.trace('registerUser Start' + JSON.stringify(req.body));
    var obj = {}, that = this, userDeviceObj = {};
    obj.email_id = req.body.email_id.toLowerCase();
    obj.password = that.encryptPassword(req.body.password);
    obj.phone_no = req.body.number;
    obj.first_name = req.body.first_name;
    obj.last_name = req.body.last_name;
    obj.act_enabled = true;
    obj.customer_pin = that.generateCustomerPin(obj.email_id);
    userDeviceObj.app_device_token_id = req.body.app_device_token_id;
    userDeviceObj.act_status = false;
    userDeviceObj.sessionId = null;
    userDeviceObj.device_type = req.body.device_type || 'IOS';

   // QueueHandler.pushNotification({
   //         message: req.body.message, // message
   //         deviceToken: req.body.deviceToken // list of divice tokens with comma seperated values
   //     });
   // QueueHandler.sendMail({
   //         to: req.body.email_id.toLowerCase(), // list of receivers
   //         subject: Globals.MailerForgotPasswordSubject, // Subject line
   //         text: "Sample Text",
   //         templateData: {
   //             logo: 'http://www.redcappi.com/asset/user_files/7667/image_bank/20160108015134.png',
   //             name: obj.email_id,
   //             ip: '192.168.2.91'
   //         },
   //         templateName: Globals.template.forgotPassword
   //     });

    that.getModels();
    that.userModel.findAll({where: {email_id: obj.email_id}})
        .then(function (arr) {
            if (arr.length === 0) {
                var user = that.userModel.build(obj);
                user.save()
                    .then(function (person) {
                        if (person) {
                            userDeviceObj.user_id = person.user_id;
                            var userDev = that.userDevModel.build(userDeviceObj);
                            userDev.save()
                                .then(function(){
                                    that.sendMail(req.headers.host, person, Globals.MailerAuthenticationSubject, {email: person.email_id, templateName: Globals.template.registration});
                                    res.send({status: 1, message: "Registration Successful. Please enter the OTP sent to your registered email to activate your Account.", user: person, sessionId: null});
                                })
                                .catch(function(err){
                                    logger.error(err.message);
                                    res.send({status: 0, message: "Data Base error: " + err.message + ". Please try again."});
                                });
                        } else {
                            res.send({status: 0, message: "Registration Failed. Please try again."});
                        }
                    });
            } else if(!arr[0].isMaster){
                res.send({status: 0, message: "You are already invited as a guest. Please login with the details sent to your registered mail."});
            } else if (!arr[0].act_authenticated) {
                that.sendMail(req.headers.host, arr[0], Globals.MailerAuthenticationSubject, {email: arr[0].email_id, templateName: Globals.template.registration});
                res.send({status: 1, message: "Please enter the authentication code sent to your registered mail.", user: arr[0], sessionId: null});
            } else {
                res.send({status: 0, message: "User already registered."});
            }
        });
    // res.send({status: 0, message: "Registration Failed. Please try again"});
    logger.trace('registerUser End');
};
User.prototype.logout = function (req, res) {
    logger.trace('logout Start' + JSON.stringify(req.body));
    var that = this
        , user_id = req.body.user_id
        , sessionId = req.body.sessionId
        , app_device_token_id = req.body.app_device_token_id;

    that.getModels();
    that.userModel.findAll({where: {user_id: user_id}})
        .then(function (arr) {
            arr = arr[0];
            if(!arr){
                res.send({status: 1, message: "logout Success."});
                logger.info("logout done");
                logger.trace('logout End');
                that.saveLogInOut("logout", user_id, app_device_token_id, null);
                return;
            }
            // if (!arr.act_enabled) {
            //     res.send({status: 0, message: "Your account has been de-activated.", active: false});
            //     logger.info("logout done");
            //     logger.trace('logout End');
            //     return ;
            // }
            that.userDevModel.findAll({where: {app_device_token_id: app_device_token_id, user_id: arr.user_id}})
                .then(function(userDev){
                    userDev = userDev[0];
                    if(userDev){
                        userDev.update({sessionId: null, act_status: false});
                    }
                    logger.info("logout done");
                    res.send({status: 1, message: "logout Success."});
                    that.saveLogInOut("logout", user_id, app_device_token_id, null);
                });
        })
        .catch(function(err){
            logger.error(err.message);
            res.send({status: 0, message: "Data Base error: " + err.message + ". Please try again."});
        });
    logger.trace('logout End');
};
User.prototype.inviteGuest = function (req, res) {
    logger.trace('inviteGuest Start' + JSON.stringify(req.body));
    var that = this
    , user_id = req.body.user_id
    , guestId = req.body.guestId.toLowerCase()
    , hub_id = req.body.hub_id
    , permissions = {};

    if(req.body.permissions){
        permissions = req.body.permissions;
    }else{
        ["device", "music", "remote", "security"].map(function(val, key){permissions[val] = req.body[val]});
    }

    var objGuest = {};
    objGuest.email_id = guestId;
    objGuest.authentication_code = that.generateToken(objGuest.email_id);
    objGuest.customer_pin = that.generateCustomerPin(objGuest.email_id);
    objGuest.password = that.encryptPassword(objGuest.authentication_code);
    objGuest.isMaster = false;

    that.getModels();
    that.userModel.findAll({where: {email_id: objGuest.email_id}})
    .then(function (guest) {
        that.userModel.findAll({where: {user_id: user_id}})
        .then(function(user){
            user = user[0];
            if (guest.length !== 0) {
                guest = guest[0];
                // if (!guest.act_enabled) {
                    // guest.update({authentication_code: objGuest.authentication_code, password: objGuest.password, isMaster: false, act_enabled: true})
                    // .then(function(gust){
                var data = {}, arr = ["device", "music", "remote", "security"];
                data.user_id = guest.user_id;
                data.hub_id = hub_id;
                data.user_type = "guest";
                data.parent_id = user_id;
                arr.map(function(val, key){data[val] = permissions[val]});
                that.userHubModel.findAll({where: {user_id: data.user_id, parent_id: data.parent_id, hub_id: data.hub_id}})
                .then(function(gustHub){
                    gustHub = gustHub[0];
                    if(gustHub){
                        if(gustHub.act_enabled){
                            res.send({status: 0, message: "Guest already invited."});
                        }else{
                            permissions.act_enabled = true;
                            gustHub.update(permissions)
                            .then(function(){
                                that.sendMail(req.headers.host, objGuest, Globals.MailerInviteGuestSubject, {email: objGuest.email_id, authCode: null, masterEmail: user.email_id, templateName: Globals.template.invitation}, objGuest.authentication_code);
                                res.send({status: 1, message: "Guest invited successfully."});
                            });
                        }
                    }else{
                        that.userHubModel.build(data).save()
                        .then(function(){
                            that.sendMail(req.headers.host, objGuest, Globals.MailerInviteGuestSubject, {email: objGuest.email_id, authCode: null, masterEmail: user.email_id, templateName: Globals.template.invitation}, objGuest.authentication_code);
                            res.send({status: 1, message: "Guest invited successfully."});
                        });
                    }
                }).catch(function(err){
                    logger.error(err.message);
                    res.send({status: 0 , message: err.message + "."});
                });
                    // });
                // } else {
                //     res.send({status: 0, message: "Guest already invited."});
                // }
            } else {
                that.userModel.build(objGuest).save()
                .then(function (newGuest) {
                    if (newGuest) {
                        var data = {}, arr = ["device", "music", "remote", "security"];
                        data.user_id = newGuest.user_id;
                        data.hub_id = hub_id;
                        data.user_type = "guest";
                        data.parent_id = user.user_id;
                        arr.map(function(val, key){data[val] = permissions[val]});
                        that.userHubModel.build(data).save()
                        .then(function(){
                            that.sendMail(req.headers.host, objGuest, Globals.MailerInviteGuestSubject, {email: objGuest.email_id, authCode: objGuest.authentication_code, masterEmail: user.email_id, templateName: Globals.template.invitation}, objGuest.authentication_code);
                            res.send({status: 1, message: "Guest invited successfully."});
                        });
                    } else {
                        res.send({status: 0, message: "Sending invitation failed. Please Try again."});
                    }
                });
            }
        });
    })
    .catch(function(err){
        logger.error(err.message);
        res.send({status: 0, message: "Data Base error: " + err.message + ". Please try again."});
    });
    logger.trace('inviteGuest End');
};
User.prototype.deleteChild = function (req, res) {
    logger.trace('deleteChild Start' + JSON.stringify(req.body));
    var that = this
        , user_id = req.body.user_id
        , guestId = req.body.guestId
        , hub_id = req.body.hub_id;

    that.getModels();
    that.userHubModel.findAll({where: {user_id: guestId, hub_id: hub_id, parent_id: user_id}})
        .then(function(hubPer){
            hubPer = hubPer[0];
            if(hubPer){
                hubPer.update({act_enabled: false, security: "0", music: "0", remote: "0", device: "0"})
                    .then(function(){
                        res.send({status: 1, message: "Deletion Success."});
                    });
            }else{
                // TODO: if record is deleted manually in database or child user is not linked exist.
                res.send({status: 0, message: "No such child exist. Please contact administrator incase of quires."});
            }
        })
        .catch(function(err){
            logger.error(err.message);
            res.send({status: 0, message: "Data Base error: " + err.message + ". Please try again."});
        });

    logger.trace('deleteChild End');
};

User.prototype.changePassword = function (req, res) {
    logger.trace('changePassword Start' + JSON.stringify(req.body));
    var that = this
        , user_id = req.body.user_id
        , currentPassword = that.encryptPassword(req.body.currentPassword)
        , password = that.encryptPassword(req.body.password);

    that.getModels();
    that.userModel.findAll({where: {user_id: user_id}})
        .then(function (user) {
            user = user[0];
            if (user.password !== currentPassword) {
                res.send({status: 0, message: "Current password is not matching."});
            }else{
                var arr = that.checkPassword(user, password);
                if (arr[0]) {
                    arr[1].act_authenticated = true;
                    user.update(arr[1]);
                    res.send({status: 1, message: "Password changed successfully."});
                } else {
                    res.send({status: 0, message: "New Password matches with one of the last three used passwords. Please enter a new password."});
                }
            }
        })
        .catch(function(err){
            logger.error(err.message);
            res.send({status: 0, message: "Data Base error: " + err.message + ". Please try again."});
        });
    logger.trace('changePassword End');
};

User.prototype.forgotPassword = function (req, res) {
    logger.trace('forgotPassword Start' + JSON.stringify(req.body));
    var that = this
        , email_id = req.body.email_id.toLowerCase();

    that.getModels();
    that.userModel.findAll({where: {email_id: email_id}})
        .then(function (user) {
            user = user[0];
            if (!user) {
                res.send({status: 0, message: "User doesn't exist."});
                return;
            }
            // if (!user.act_enabled) {
            //     res.send({status: 0, message: "Your account has been de-activated.", active: false});
            //     return;
            // }
            var newPW = that.generateToken(user.email_id);
            that.sendMail(req.headers.host, user, Globals.MailerForgotPasswordSubject, {email: user.email_id, password: newPW, templateName: Globals.template.forgotPassword}, newPW);
            // var arr = that.checkPassword(user, that.encryptPassword(newPW));
            // user.update(arr[1]);
            user.update({otp: that.encryptPassword(newPW)});
            res.send({status: 1, message: "Password has been reset and sent to your registered mail successfully."});
        })
        .catch(function(err){
            logger.error(err.message);
            res.send({status: 0, message: "Data Base error: " + err.message + ". Please try again."});
        });
    logger.trace('forgotPassword End');
};
User.prototype.getPermissions = function (req, res) {
    logger.trace('getPermissions Start' + JSON.stringify(req.body));
    var that = this
        , user_id = req.body.user_id
        , hub_id = req.body.hub_id;

    that.getModels();
    that.userHubModel.findAll({attributes: ["device", "security", "music", "remote", "user_type"], where: {hub_id: hub_id, user_id: user_id}})
        .then(function (hubPer){
            hubPer = hubPer[0];
            if(!hubPer){
                res.send({status: 0, message: "You have no permissions in this hub."});
            }else{
                res.send({status: 1, message: "fetched permissions successfully.", user: hubPer});
            }
        })
        .catch(function (err){
            logger.error(err.message);
            res.send({status: 0, message: "Data Base error: " + err.message + ". Please try again."});
        });
    logger.trace('getPermissions End');
};
User.prototype.updatePermissions = function (req, res) {
    logger.trace('updatePermissions Start' + JSON.stringify(req.body));
    var that = this
        , user_id = req.body.user_id
        , guestId = req.body.guestId
        , hub_id = req.body.hub_id
        , permissions = req.body.permissions;

    that.getModels();
    that.userHubModel.update(permissions, {where: {user_id: guestId, hub_id: hub_id, parent_id: user_id}})
        .then(function (userhub) {
            if(userhub[0]){
                res.send({status: 1, message: "Permissions updated successfully."});
            }else{
                res.send({status: 0, message: "Guest is not invited by you for this hub. Please contact admin."});
            }
        })
        .catch(function (err){
            logger.error(err.message);
            res.send({status: 0, message: "Data Base error: " + err.message + ". Please try again."});
        });
    logger.trace('updatePermissions End');
};
User.prototype.getGuestUsers = function (req, res) {
    logger.trace('getGuestUsers Start' + JSON.stringify(req.body));
    var that = this
        , user_id = req.body.user_id
        , hub_id = req.body.hub_id;

    that.getModels();

    that.userHubModel.findAll({attributes: ["device", "music", "user_id", "remote", "security", "user_type"], order: 'user_id DESC', where: {parent_id: user_id, hub_id: hub_id, act_enabled: true}})
        .then(function (userhub) {
            if (userhub.length === 0) {
                res.send({status: 0, message: "No Child User."});
            } else {
                var arrUser = _.pluck(userhub, "user_id");
                that.userModel.findAll({attributes: ["user_id", "email_id"], where: {user_id: {$in : arrUser}}, order: 'user_id DESC'})
                    .then(function(usrArr){
                        for(var i = 0; i < userhub.length; i++){
                            userhub[i].dataValues.email_id = usrArr[i].email_id;
                        }
                        userhub = _.chain(userhub).pluck("dataValues").sortBy('email_id').value();
                        res.send({status: 1, message: "Got the Guest users.", users: userhub});
                    });
            }
        })
        .catch(function (err){
            logger.error(err.message);
            res.send({status: 0, message: "Data Base error: " + err.message + ". Please try again."});
        });
    logger.trace('getGuestUsers End');
};

User.prototype.sendMail = function (host, user, subject, obj, _authentication_code) {
    var that = this;
    obj.logo = 'http://' + host + "" + Globals.MailLogo;
    obj.customer_pin = user.customer_pin; // customer id

    if(!_authentication_code){
        var authentication_code = that.generateToken(user.email_id);
        user.update({authentication_code: authentication_code})
            .catch(function (err){
                logger.error(err.message);
            });
        obj.authCode = authentication_code;
    }
    obj.greeting = "Hello"
    QueueHandler.sendMail({
        to: user.email_id, // list of receivers
        subject: subject, // Subject line
        templateData: obj,
        templateName: obj.templateName
    });
};
User.prototype.checkPassword = function(user, newPW) {
    var columnsArr = ['password', 'old_password1', 'old_password2'];
    var arr = (columnsArr.map(function (key) {
        return user[key];
    })).filter(function (n) {
        return (n !== undefined || n !== null);
    });
    var bool = (arr.indexOf(newPW) === -1);
    arr.unshift(newPW);
    arr = arr.splice(0, 3);
    var obj = {};
    arr.map(function (val, index) {
        obj[columnsArr[index]] = val;
    });
    return [bool, obj];
};

User.prototype.generateToken = function(id) {
    // getrating the token Logic
    return speakeasy.totp({key: (id + Math.random())});
};

User.prototype.generateCustomerPin = function(id) {
    // getrating the customer pin Logic
    return speakeasy.totp({key: id, length: 8});
};

User.prototype.encryptPassword = function(id) {
    var sha = crypto.createHash('md5');
    sha.update(id);
    return sha.digest('hex');
};
User.prototype.generateSessionId = function() {
    var sha = crypto.createHash('sha1');
    sha.update(Math.random().toString());
    return sha.digest('hex');
};

User.prototype.SetUserDev = function(app_device_id, user, sessionId, res, message, act_authenticated, device_type, isWebLogin) {
  //  logger.info(app_device_id+ ' : ' +user.user_id + ' : ' + sessionId);
    var that = this;
    this.userDevModel.findAll({where: {user_id: user.user_id, app_device_token_id: app_device_id}})
        .then(function (record){
            record = record[0];
            var act_status = (sessionId ? true : false);
            var obj = {
                app_device_token_id: app_device_id,
                user_id: user.user_id,
                act_status: act_status,
                sessionId: sessionId,
                device_type: device_type || 'IOS',
            };
            // if(act_status && !isWebLogin){
            //   obj.otp = null;
            // }
            if(record){
                record.update({act_status: obj.act_status, sessionId: sessionId, device_type: device_type})
                    .then(function(){
                        if(res){
                            that.onLoginSuccess(res, user, message, act_authenticated, sessionId, (act_status && !isWebLogin));
                            that.saveLogInOut("login", user.user_id, app_device_id, sessionId);
                        }
                    });
            }else{
                var userDev = that.userDevModel.build(obj);
                userDev.save()
                    .then(function(){
                        if(res){
                            that.onLoginSuccess(res, user, message, act_authenticated, sessionId, (act_status && !isWebLogin));
                            that.saveLogInOut("login", user.user_id, app_device_id, sessionId);
                        }
                    })
                    .catch(function(err){
                        logger.error(err.message);
                    });
            }
        })
        .catch(function (err){
            logger.error(err.message);
        });
};

User.prototype.saveLogInOut = function(_collection, user_id, app_device_token_id, sessionId){
    var obj = {
        user_id: user_id,
        app_device_token_id: app_device_token_id,
        sessionId: sessionId,
        timestamp: new Date()
    };
    mongodb.save(_collection, obj, function(err, data){
        if(err){
            logger.info("some problem in saving " + _collection + " details: " + err);
        }else{
            logger.info("saved " + _collection + " details: " + JSON.stringify(data));
        }
    });
};

User.prototype.onLoginSuccess = function(res, user, message, act_authenticated, sessionId, nullOtp) {
    var obj = {act_authenticated: act_authenticated, authentication_code: null};
    if(nullOtp){
      obj.otp = null
    }
    user.update(obj)
        .then(function(person){
            res.send({status: 1, message: message, user: person, sessionId: sessionId});
        });
};

User.prototype.validateSession = function(req, res, next){
    var that = this
    , user_id = req.body.user_id
    , hub_id = req.body.hub_id
    , app_device_token_id = req.body.app_device_token_id
    , sessionId = req.body.sessionId
    , origin_id = req.body.origin_id
    , type = req.body.device_type; // Alexa validateSession

//    logger.trace("url: " + req.url + ": validateSession Start: "+ JSON.stringify(req.body));
    that.getModels();

   if(type !== undefined && type === 'ALEXA'){
      //TODO: Alexa session validate process.
      if(sessionId !== undefined && sessionId.length > 5){
        //TODO: validate the sessionid
        // loggers.error("comming here");
	var alexa_session_id = user_id+"_"+sessionId+"_"+hub_id;
        var alexa = { session_id: alexa_session_id };
        that.alexaUserModel.findAll({where: alexa})
            .then(function (alexaUserArray) {
                if (alexaUserArray.length === 1) {
  //                logger.trace("validateSession End");
                  next();
                }else{
                  logger.error("Morethan one account exists with given session_id : "+sessionId);
                  res.send({status: 0, message: "Unauthorized authentication failed."});
                }
            })
            .catch(function(err){
                logger.error(err.message);
                res.send({status: 0, message: "Unauthorized authentication failed."});
            });

      }else{
          res.send({status: 0, message: "Unauthorized authentication failed."});
      }
    }else{
      //TODO: Regular process

    if(typeof origin_id === "undefined"){
        res.send({status: 0, message: "Unauthorized authentication failed."});
        return;
    }
    if(Utils.isOriginHub(origin_id)){
        var redisKey = "hub_id_"+hub_id;
        redisDb.isKeyExists(redisKey, function(bool){
            if(bool){
                redisDb.getData(redisKey, function(err, data){
                    if(err){
                        res.send({status: 0, message: "Something went wrong. Please contact admin"});
                    }else{
                        if(data.sessionId === sessionId){
                            next();
                        }else{
                            res.send({status: 0, message: "Unauthorized authentication failed."});
                        }
                    }
                });
            }else{
                res.send({status: 0, message: "Unauthorized authentication failed."});
            }
        });
    }else if(Utils.isOriginApp(origin_id)){
        that.userModel.findAll({where: {user_id: user_id}})
            .then(function (user) {
                user = user[0];
                if (!user) {
                    res.send({status: 0, message: "User doesn't exist."});
                    return;
                }
                // if (!user.act_enabled) {
                //     res.send({status: 0, message: "Your account has been de-activated.", active: false});
                //     return;
                // }
                that.userDevModel.findAll({where: {app_device_token_id: app_device_token_id, user_id: user_id}})
                    .then(function(userDev){
                        userDev = userDev[0];
                        if(userDev && userDev.sessionId === sessionId){
                            that.userHubModel.findAll({where: {user_id: user_id, hub_id: hub_id, act_enabled: true}})
                                .then(function(userHubs){
                                    if(['/addHub', '/addNewHub', '/getHubs', '/changePassword', '/getAllUserDetails'].indexOf(req._parsedUrl.pathname) === -1 && userHubs.length === 0){
                                        res.send({status: 0, message: "Your account has been de-activated in this hub.", active: false});
                                        return ;
                                    }
    //                                logger.trace("validateSession End");
                                    if(!req.body.password)
                                    {
                                    req.body.password = user.password;
                                    }
                                    next();
                                });
                        }else{
                            res.send({status: 0, message: "Unauthorized authentication failed."});
                        }
                    });
            })
            .catch(function(err){
                res.send({status: 0, message: err.message });
            });
    }else{
        // if called from support site
        next();
    }
  }
};

User.prototype.getModels = function(){
    if(this.userModel){
        return ;
    }
    var Models = db.getModels();
    this.userModel = Models.User;
    this.userHubModel = Models.UserHub;
    this.userDevModel = Models.UserDev;
    this.hubModel = Models.Hub;
    this.hubDefModel = Models.HubDef;
    this.roomModel = Models.Room;
    this.hubRoomModel = Models.HubRoom;
    this.hubEmergConts = Models.HubEmergConts;
    this.deviceModel = Models.Devices;
    this.remoteModel = Models.RemoteKeyInfo;
    this.alexaUserModel =Models.AlexaUser;
};

User.prototype.deleteUserDetails = function(req, res) {
    var that = this
        , email_id = req.body.email_id;

    var redisKey = "OTP_delete";
    redisDb.isKeyExists(redisKey, function(bool){
        if(bool){
            redisDb.getData(redisKey, function(err, data){
                logger.info("JSON.stringify(data)");
                logger.info(JSON.stringify(data));
                if(err || data.OTP != req.body.otp){
                    res.send({status: 0, message: "This operation is Unauthorized."});
                }else{
                    redisDb.setData("OTP_delete", {OTP: "test@blaze123217"}, function(err, obj){
                        if(err){
                            logger.info("redisKey reset is not done.");
                        }else{
                            logger.info("redisKey reset is done.");
                        }
                    });
                    that.getModels();
                    that.userModel.findAll({where: {email_id: email_id}})
                        .then(function(user){
                            if(user.length === 0){
                                res.send({status: 0, message: "User doesn't exist." });
                                return;
                            }
                            var user_id = user[0].user_id;
                            that.userDevModel.destroy({where: {user_id: user_id}})
                                .then(function(){
                                    that.userHubModel.findAll({where: {user_id: user_id, user_type: 'master'}})
                                        .then(function(userHubs){
                                            if(userHubs.length === 0){
                                                that.userModel.destroy({where: {user_id: user_id}})
                                                    .then(function(){
                                                        res.send({status: 1, message: "User details deleted successfully."});
                                                    });
                                                return;
                                            }
                                            var hubs = _.pluck(userHubs, "hub_id");
                                            var _collections = ["smsState", "events", "states", "hubStates"];
                                            _.map(_collections, function(v, i){
                                                mongodb.deleteManyByObject(v, {hub_id: { $in: hubs }}, function(){});
                                            });
                                            that.userHubModel.destroy({where: {hub_id: {$in: hubs}}})
                                                .then(function(){
                                                    logger.info("deleted user hubs Relations.");
                                                });
                                            that.userModel.destroy({where: {user_id: user_id}})
                                                .then(function(){
						 //Deleting the alexa user.
	                        	            that.alexaUserModel.destroy({where: {user_id: user_id}})
        	                                      .then(function(){
                	                                logger.info("deleted user alexa details.");
                        	                      });
                                                    logger.info("deleted user.");
                                                });
                                            that.hubModel.destroy({where: {hub_id: {$in: hubs}}})
                                                .then(function(){
                                                    logger.info("deleted hubs of the user.");
                                                });
                                            that.hubDefModel.destroy({where: {hub_id: {$in: hubs}}})
                                                .then(function(){
                                                    logger.info("deleted hub definition of the user.");
                                                });
                                            that.hubEmergConts.destroy({where: {hub_id: {$in: hubs}}})
                                                .then(function(){
                                                    logger.info("deleted hub Emergency Contacts of the user.");
                                                });
                                            that.hubRoomModel.findAll({where: {hub_id: {$in: hubs}}})
                                                .then(function(hubRooms){
                                                    var rooms = _.pluck(hubRooms, "room_id");
                                                    that.hubRoomModel.destroy({where: {hub_id: {$in: hubs}}})
                                                        .then(function(){
                                                            logger.info("deleted hub rooms Relations of the user.");
                                                        });
                                                    that.roomModel.destroy({where: {room_id: {$in: rooms}}})
                                                        .then(function(){
                                                            logger.info("deleted hub rooms of the user.");
                                                        });
                                                    that.deviceModel.findAll({where: {room_id: {$in: rooms}}})
                                                        .then(function(devices){
                                                            if(devices.length === 0){
                                                                logger.info("no devices exist for the user.");
                                                                return;
                                                            }
                                                            var device_ids = _.pluck(devices, "device_id");
                                                            that.deviceModel.destroy({where: {room_id: {$in: rooms}}})
                                                                .then(function(){
                                                                    logger.info("deleted devices of the user.");
                                                                });
                                                            that.remoteModel.destroy({where: {device_id: {$in: device_ids}}})
                                                                .then(function(){
                                                                    logger.info("deleted remotes of the user.");
                                                                });
                                                        });
                                                    res.send({status: 1, message: "User details deleted successfully."});
                                                });
                                        });
                                });
                        })
                        .catch(function(err){
                            res.send({status: 0, message: err.message });
                        });
                }
            });
        }else{
            res.send({status: 0, message: "This operation is Unauthorized."});
        }
    });
};

User.prototype.sendOTP = function(req, res) {
    var OTP = this.generateToken(Math.random().toString());

    redisDb.setData("OTP_delete", {OTP: OTP}, function(err, obj){
        if(err){
            res.send({status: 0, message: "Unable to process the operation please try again."});
        }else{
            QueueHandler.sendMail({
                to: Globals.adminMail, // list of receivers
                subject: "OTP for the delete user request is: " + OTP + "." // Subject line
            });
            res.send({status: 1, message: "Mail has been sent to the admin mail."});
        }
    });
};

User.prototype.sendOTPToMasterUser = function(req, res) {
    var OTP = this.generateToken(Math.random().toString());
    var that = this;
    that.getModels();

    that.userModel.findAll({where: {user_id: req.body.user_id}})
        .then(function(user){
            if(user.length === 0){
                res.send({status: 0, message: "User doesn't exist." });
                return;
            }else{
              redisDb.setData("OTP_master_hub_delete_"+req.body.user_id, {OTP: OTP}, function(err, obj){
                  if(err){
                      res.send({status: 0, message: "Unable to process the operation please try again."});
                  }else{
                      QueueHandler.sendMail({
                          to: user[0].email_id, // list of receivers
                          subject: "OTP for the delete hub request is: " + OTP + "." // Subject line
                      });
                      res.send({status: 1, message: "Mail has been sent to you.", otp: OTP});
                  }
              });
            }
          }).catch(function(err){
              res.send({status: 0, message: err.message });
          });

};

User.prototype.deleteMasterUserHub = function(req, res){
  var that = this
      , user_id = req.body.user_id
      , hub_id = req.body.hub_id;

  var redisKey = "OTP_master_hub_delete_"+user_id;
  redisDb.isKeyExists(redisKey, function(bool){
      if(bool){
          redisDb.getData(redisKey, function(err, data){
              logger.info("JSON.stringify(data)");
              logger.info(JSON.stringify(data));
              if(err || data.OTP != req.body.otp){
                  res.send({status: 0, message: "This operation is Unauthorized."});
              }else{
                  redisDb.setData(redisKey, {OTP: "test@blaze123217"}, function(err, obj){
                      if(err){
                          logger.info("redisKey reset is not done.");
                      }else{
                          logger.info("redisKey reset is done.");
                      }
                  });
                  that.getModels();
                  that.userHubModel.findAll({where: {user_id: user_id, hub_id: hub_id, user_type: 'master'}})
                      .then(function(userHubs){
                          if(userHubs.length === 0){
                              res.send({status: 1, message: "Hub deleted successfully."});
                              return;
                          }
                          var _collections = ["smsState", "events", "states", "hubStates"];
                          _.map(_collections, function(v, i){
                              mongodb.deleteManyByObject(v, {hub_id: hub_id}, function(){});
                          });
                          that.userHubModel.destroy({where: {hub_id: hub_id}})
                              .then(function(){
                                  logger.info("deleted user hubs Relations.");
                              });
                          that.hubModel.destroy({where: {hub_id: hub_id}})
                              .then(function(){
                                  logger.info("deleted hubs of the user.");
                              });
                          that.hubDefModel.destroy({where: {hub_id: hub_id}})
                              .then(function(){
                                  logger.info("deleted hub definition of the user.");
                              });
                          that.hubEmergConts.destroy({where: {hub_id: hub_id}})
                              .then(function(){
                                  logger.info("deleted hub Emergency Contacts of the user.");
                              });
                          that.hubRoomModel.findAll({where: {hub_id: hub_id}})
                              .then(function(hubRooms){
                                  var rooms = _.pluck(hubRooms, "room_id");
                                  that.hubRoomModel.destroy({where: {hub_id: hub_id}})
                                      .then(function(){
                                          logger.info("deleted hub rooms Relations of the user.");
                                      });
                                  that.roomModel.destroy({where: {room_id: {$in: rooms}}})
                                      .then(function(){
                                          logger.info("deleted hub rooms of the user.");
                                      });
                                  that.deviceModel.findAll({where: {room_id: {$in: rooms}}})
                                      .then(function(devices){
                                          if(devices.length === 0){
                                              logger.info("no devices exist for the user.");
                                              return;
                                          }
                                          var device_ids = _.pluck(devices, "device_id");
                                          that.deviceModel.destroy({where: {room_id: {$in: rooms}}})
                                              .then(function(){
                                                  logger.info("deleted devices of the user.");
                                              });
                                          that.remoteModel.destroy({where: {device_id: {$in: device_ids}}})
                                              .then(function(){
                                                  logger.info("deleted remotes of the user.");
                                              });
                                      });
                                  res.send({status: 1, message: "Hub deleted successfully."});
                              });
                      })
                      .catch(function(err){
                          res.send({status: 0, message: err.message });
                      });
              }
          });
      }else{
          res.send({status: 0, message: "This operation is Unauthorized."});
      }
  });
};

User.prototype.updateUserAddress = function(req, res){
  var that = this
      , user_id = req.body.user_id
      , address = req.body.address;

  that.getModels();
  that.userModel.findAll({where: {user_id: user_id}})
    .then(function(user){
      if(user.length == 0){
        res.send({status: 0, message: "User Does not exist."});
      }else {
        user[0].update({address: address});
        res.send({status: 1, message: "User address updated successfully."});
      }
    })
    .catch(function(err){
        res.send({status: 0, message: err.message });
    });
};

User.prototype.loginWebUser = function (req, res) {
    logger.trace('loginWebUser Start' + JSON.stringify(req.body));
    var obj = {"customer_pin": req.body.customer_pin}, that = this, app_device_token_id = req.body.app_device_token_id;
    that.getModels();

    that.userModel.findAll({where: obj})
        .then(function (arr) {
            if (arr.length === 0) {
                res.send({status: 0, message: "User does not exist."});
            }
            else {
                   arr = arr[0];


                    if (!arr.act_authenticated)
                    {
                        if (arr.isMaster) {
                            that.sendMail(req.headers.host, arr, Globals.MailerAuthenticationSubject, {email: arr.email_id, templateName: Globals.template.registration});
                            res.send({status: 1, message: ("Please enter the authentication code sent to your registered email."), user: arr, sessionId: null});
                        }
                    }

                     else {
                        var sessionId = that.generateSessionId();
                        that.SetUserDev(app_device_token_id, arr, sessionId, res, "Login Success.", true, req.body.device_type, true);
                    }
              }
        })
        .catch(function(err){
            logger.error(err.message);
            res.send({status: 0, message: "Data Base error: " + err.message + ". Please try again."});
        });
    logger.trace('loginUser End');
};

  User.prototype.getallcustomers = function(req, res){


        logger.trace('customerlist Start');
  var that=this;
  that.getModels();
 that.userModel.belongsTo(that.userHubModel, {foreignKey: 'user_id', targetKey: 'user_id'});
 that.userHubModel.belongsTo(that.hubModel, {foreignKey: 'hub_id', targetKey: 'hub_id'});
 that.hubModel.belongsTo(that.hubDefModel,{foreignKey: 'hub_id', targetKey: 'hub_id'});
  that.userModel.findAll({
  where: {isMaster:1},
  include: [{
    model: that.userHubModel,
    include: [{
    model: that.hubModel,
    attributes: ['hub_status','hub_status_date','hub_id'],
    include:[{
     model: that.hubDefModel,
    attributes: ['hub_longitude','hub_latitude']
}]


  }]

  }]
})
      .then(function (arr) {
          if (arr.length === 0) {
              res.send({status: 0, message: "Users does not exist."});
          } else {



               res.send({status: 1, message: "user details", user: arr});

          }
      })
      .catch(function(err){
          logger.error(err.message);
          res.send({status: 0, message: "Data Base error: " + err.message + ". Please try again."});
      });
  logger.trace('customerlist End');

};

module.exports = new User();
