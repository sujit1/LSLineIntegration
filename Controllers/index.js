var express = require('express')
	, Globals = require('../globals')
	, router = express.Router();

router.use('/user', require('./User/Users').router);
router.use('/device', require('./Device/Device'));
router.use('/category', require('./DeviceCategory/DeviceCategory'));
router.use('/hub', require('./Hub/Hubs'));
router.use('/room', require('./Room/Room'));
router.use('/commons', require('./Commons/Utils').router);
router.use('/event', require('./Events/Events'));
router.use('/customerCare', require('./CustomerCare/CustomerCare'));
router.use('/auth', require('./User/Auth'));
router.use('/webhook',require('./Webhook/webhook'));

// router.get('/', function(req, res) {
//   res.render('index');
// });

router.get('/', function(req, res) {
	res.sendFile(Globals.appRoot + '/public/views/layout.html');
});

router.get('/customercare', function(req, res) {
	res.sendFile(Globals.appRoot + '/public/views/customerCare.html');
});

module.exports = router;
