var express = require('express')
    , router = express.Router()
    , _ = require('underscore')
    , db = require('../../mySqlDB')
    , Sequelize = require('sequelize')
    , User = require('../User/Users')
    , Globals = require('../../globals')
    , log4js = require('log4js');

log4js.configure(Globals.LoggerConfig);
var logger = log4js.getLogger('relative-logger');

function Room() {
    _.bindAll(this, "addRoom", "addRoomToHub", "getRoomList", "deleteRoom", "updateRoom", "getModels");

    router.post('/addRoom', User.validateSession, this.addRoom);
    router.post('/getRoomList', User.validateSession, this.getRoomList);
    router.post('/deleteRoom', User.validateSession, this.deleteRoom);
    router.post('/updateRoom', User.validateSession, this.updateRoom);

    return router;
};

Room.prototype.addRoom = function(req, res) {
    var that = this
        , hub_id = req.body.hub_id
        , obj = {};
    obj.room_name = req.body.room_name;
    obj.room_description = req.body.room_description;

    that.getModels();
    that.hubModel.findAll({where: {hub_id: hub_id}})
        .then(function(hubArr){
            if(hubArr.length === 0){
                res.send({status: 0, message: "Hub doesn't exists."});
            }else{
                that.hubRoomModel.findAll({attributes: ["room_id"], where: {hub_id: hub_id}})
                    .then(function(hubRoomArr){
                        if(hubRoomArr.length === 0){
                            that.addRoomToHub(res, obj, hub_id);
                        }else{
                            var roomsIds = _.pluck(hubRoomArr, "room_id");
                            that.roomModel.findAll({attributes: ["room_name"], where: {room_id: {$in: roomsIds}}})
                                .then(function(rooms){
                                    var roomNames = _.pluck(rooms, "room_name");
                                    if(roomNames.indexOf(obj.room_name) === -1){
                                        that.addRoomToHub(res, obj, hub_id);
                                    }else{
                                        res.send({status: 0, message: "Room Name already exists. Try another name"});
                                    }
                                });
                        }
                    });
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message });
        });
};

Room.prototype.addRoomToHub = function(res, obj, hub_id){
    var that = this;
    that.roomModel.build(obj).save()
        .then(function(room){
            that.hubRoomModel.build({hub_id: hub_id, room_id: room.room_id}).save()
                .then(function(){
                    res.send({status: 1, message: "Room added successfully.", room_id: room.room_id });
                });
        });
};

Room.prototype.getRoomList = function(req, res) {
    var that = this
        , hub_id = req.body.hub_id;

    that.getModels();
    that.hubRoomModel.findAll({attributes: ['room_id'] , where: {hub_id: hub_id}})
        .then(function(roomsIds){
            if(roomsIds.length === 0){
                res.send({status: 0, message: "No Rooms for the selected hub."});
            }else{
                var arr = _.pluck(roomsIds, 'room_id');
                that.roomModel.findAll({attributes: ['room_id', 'room_name'], where: {room_id: {$in: arr}}})
                    .then(function(rooms){
                        if(rooms.length === 0){
                            res.send({status: 0, message: "No Rooms for the selected hub."});
                        }else{
                            res.send({status: 1, message: "Getting rooms is success.", rooms: rooms, count: rooms.length});
                        }
                    });
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message });
        });
};

Room.prototype.deleteRoom = function(req, res) {
    logger.trace('deleteRoom Start: ' + JSON.stringify(req.body));
    var that = this
        , user_id = req.body.user_id
        , room_id = req.body.reqObject.room_id;

    that.getModels();
    that.roomModel.findAll({where: { room_id: room_id }})
        .then(function(rooms){
            if(rooms[0]){
                that.deviceModel.findAll({where: {room_id: room_id, device_state: "ADDED"}})
                  .then(function(devs){
                    if(devs[0]){
                      res.send({status: 0, message: "Room cannot be deleted. Please try after deleting all devices in the room."});
                      return false;
                    }
                    that.hubRoomModel.update({room_enabled: false}, {where: { room_id: room_id }})
                        .then(function(){
                            res.send({status: 1, message: "Room deleted successfully."});
                            logger.trace('deleteRoom End');
                        });
                  });

            }else{
                res.send({status: 0, message: "No Room is present with given id."});
                logger.trace('deleteRoom End');
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message });
        });
};

Room.prototype.updateRoom = function(req, res) {
    logger.trace('updateRoom Start: ' + JSON.stringify(req.body));
    var that = this
        , obj = req.body.reqObject
        , room_id = req.body.reqObject.room_id;

    delete obj.room_id;
    // obj.room_name = req.body.reqObject.room_name;
    // obj.room_description = req.body.reqObject.room_description;

    that.getModels();
    that.roomModel.update(obj, {where: { room_id: room_id }})
        .then(function(result){
            if(result === 0){
                res.send({status: 1, message: "No room is present with the given id."});
                logger.trace('deleteRoom End');
            }else{
                res.send({status: 1, message: "Room is updated successfully."});
                logger.trace('deleteRoom End');
            }
        })
        .catch(function(err){
            res.send({status: 0, message: err.message });
        });
};

Room.prototype.getModels = function(){
    if(this.roomModel){
        return ;
    }
    var Models = db.getModels();
    this.roomModel = Models.Room;
    this.hubModel = Models.Hub;
    this.hubRoomModel = Models.HubRoom;
    this.deviceModel = Models.Devices;
};

module.exports = new Room();
